import { forwardRef, useImperativeHandle, useRef, useState } from "react";
import { FormControl, InputGroup, Row } from "react-bootstrap";

const InputRowComponent = forwardRef((props, ref) => {
	const [isInvalid, setIsInvalid] = useState(false);

	const refInput = useRef(null);

	const value = () => refInput.current.value;

	const isValid = () => {
		setIsInvalid(
			props.patron ? !props.patron.test(value()) : value() === ""
		);
		return props.patron ? props.patron.test(value()) : value() !== "";
	};

	useImperativeHandle(ref, () => ({
		value,
		isValid,
	}));

	return (
		<Row>
			<InputGroup>
				<InputGroup.Text>{props.label}</InputGroup.Text>
				<FormControl
					type={props.type ?? "text"}
					ref={refInput}
					defaultValue={props.value ?? ""}
					isInvalid={isInvalid}
					placeholder={props.placeholder ?? ""}
				/>
			</InputGroup>
		</Row>
	);
});

export default InputRowComponent;
