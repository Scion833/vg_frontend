import { Accordion, Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

const AccordionLink = (props) => {
	const pageList = props.list ?? [];

	return (
		<Accordion bsPrefix="accordion-barAction">
			<Accordion.Item eventKey="0">
				<Accordion.Header>{props.name}</Accordion.Header>
				<Accordion.Body>
					{pageList.map((value, index) => (
						<Row
							className="btn-ayuda text-start text-decoration-none reactive-menu"
							as={Link}
							key={props.name+""+index}
							to={value.url}
						>
							<Col>
								<p className="text-ayuda text-dark-emphasis">
									{value.name}
								</p>
							</Col>
							<Col lg="2">{value.icon ?? <></>}</Col>
						</Row>
					))}
				</Accordion.Body>
			</Accordion.Item>
		</Accordion>
	);
};

export default AccordionLink;
