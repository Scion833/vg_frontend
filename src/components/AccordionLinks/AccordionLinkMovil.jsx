import {  NavDropdown } from "react-bootstrap";
import { Link } from "react-router-dom";

const AccordionLinkMovil = (props) => {
	

	return (
		<NavDropdown className="reactive-navbar" title={props.name}>
			{props?.list.map((obj, index) => (
				<NavDropdown.Item
					as={Link}
					to={obj.url}
					key={"navbar" + props.name + index}
					onClick={() => props.offcanvas()}
				>
					{obj.name}
				</NavDropdown.Item>
			))}
		</NavDropdown>
	);
};

export default AccordionLinkMovil;
