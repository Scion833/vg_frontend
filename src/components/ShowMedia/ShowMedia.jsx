import { useEffect, useState } from "react";
import VideoPlayer from "./VideoPlayer";
import ImageMedia from "./ImageMedia";
import AudioPlayer from "./AudioPlayer";

const ShowMedia = (props) => {
	const [type, setType] = useState("");

	const getMedia = (t) => {
		if (t === "mp4" || t === "avi" || t === "ogv" || t === "mkv") {
			return <VideoPlayer namefile={props.namefile} />;
		}
		if (
			t === "apng" ||
			t === "png" ||
			t === "gif" ||
			t === "ico" ||
			t === "cur" ||
			t === "jpg" ||
			t === "jpeg" ||
			t === "jfif" ||
			t === "pjpeg" ||
			t === "pjp" ||
			t === "pnp" ||
			t === "svg"
		) {
			return <ImageMedia namefile={props.namefile} />;
		}
		if (t === "ogg" || t === "mp3" || t === "wav") {
			return <AudioPlayer namefile={props.namefile} type={t} />;
		}
		return <></>;
	};

	useEffect(() => {
		const getType = () => {
			let part = props.namefile?.split(".") ?? [];
			setType(part[part.length > 0 ? part.length - 1 : 0]);
		};
		getType();
		// eslint-disable-next-line
	}, []);
	return <>{getMedia(type)}</>;
};

export default ShowMedia;
