import { useEffect, useRef, useState } from "react";
import { DIRURL, static_files } from "../../models/coneccion/comunicacion";

const AudioPlayer = (props) => {
	const refAudio = useRef(null);

	const [url, setUrl] = useState("");

	const getAudio = async () => {
		await fetch(
			DIRURL.concat(
				static_files.path_pruebas + "?nfile=" + props.namefile
			),
			{
				method: "GET",
				headers: {
					Accept: "*/*",
					"x-token": localStorage.getItem("x-token"),
				},
			}
		)
			.then((res) => res.blob())
			.then((data) => {
				setUrl(URL.createObjectURL(data));
				
				//refAudio.current.src = URL.createObjectURL(data);
				//refAudio.current.type = getType();
			})
			.catch((err) => console.log(err));
	};

	/* const getType = () => {
		switch (props.type) {
			case "mp3":
				return "audio/mpeg";
			case "ogg":
				return "audio/ogg";
			case "wav":
				return "audio/wav";
			default:
				break;
		}
	}; */

	useEffect(() => {
		getAudio();
		//eslint-disable-next-line
	}, []);

	return (
		<audio ref={refAudio} src={url} controls>
			{/* <source ref={refAudio} src={url} type={type} /> */}
		</audio>
	);
};

export default AudioPlayer;
