import { Image } from "react-bootstrap";
import { DIRURL, static_files } from "../../models/coneccion/comunicacion";
import { useEffect, useState } from "react";

const ImageMedia = (props) => {
	const [img, setImg] = useState(null);
	const getImage = async () => {
		await fetch(
			DIRURL.concat(
				static_files.path_pruebas + "?nfile=" + props.namefile
			),
			{
				method: "GET",
				headers: {
					"x-token": localStorage.getItem("x-token"),
					Accept: "*/*",
				},
			}
		)
			.then((res) => res.blob())
			.then((data) => {
				setImg(URL.createObjectURL(data));
			})
			.catch((err) => console.log(err));
	};

	useEffect(() => {
		getImage();
		// eslint-disable-next-line
	}, []);

	return (
		<div>
			<Image width={"320px"} height={"240px"} src={img} />
		</div>
	);
};

export default ImageMedia;
