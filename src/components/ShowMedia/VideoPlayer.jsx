import React, { useEffect, useRef } from "react";
import { DIRURL, static_files } from "../../models/coneccion/comunicacion";

function VideoPlayer(props) {
	//type: "video/mp4",

	const refVideoElement = useRef(null);

	const getVideo = () => {
		fetch(
			DIRURL.concat(
				static_files.path_pruebas + "?nfile=" + props.namefile
			),
			{
				method: "GET",
				headers: {
					"x-token": localStorage.getItem("x-token"),
					Accept: "*/*",
				},
			}
		)
			.then((res) => res.blob())
			.then((data) => {
				refVideoElement.current.src = URL.createObjectURL(data);
				//setVideoSources(URL.createObjectURL(data))
			})
			.catch((err) => {
				console.log(err);
			});
	};

	useEffect(() => {
		if (props.namefile) {
			getVideo();
		}
		//eslint-disable-next-line
	}, []);

	return (
		<div>
			<video width={"320"} height={"240"} ref={refVideoElement} controls>
				Tu navegador no soporta la reproducción de video.
			</video>
		</div>
	);
}

export default VideoPlayer;
