import { useEffect, useState } from "react";
import { Toast } from "react-bootstrap";
import { Link } from "react-router-dom";
import { DIRURL, urls_apis } from "../../models/coneccion/comunicacion";

const ModelToast = (props) => {
	const [show, setShow] = useState(true);
	const [user, setUser] = useState(null);

	const getUsuarioByIDForAlert = async () => {
		await fetch(DIRURL.concat(urls_apis.getUsuarioByIDForAlert), {
			method: "GET",
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
				"Content-Type": "application/json",
			},
		})
			.then((res) => {
				if (res.status === 200) {
					return res.json();
				}
			})
			.then((data) => {
				setUser(data.data);
			})
			.catch((err) => {
				console.log(err);
			});
	};

	useEffect(() => {
		getUsuarioByIDForAlert();
	}, []);

	return (
		<Toast
			onClose={() => {
				setShow(false);
				props.onClose();
			}}
			show={show}
		>
			<Toast.Header>
				<strong className="me-auto">{props.title}</strong>
				<small className="text-muted"></small>
			</Toast.Header>
			<Toast.Body
				as={Link}
				className="fw-light text-decoration-none"
				to={
					"EntidadAyuda/Mapa?latitud=" +
					props.latitud +
					"&longitud=" +
					props.longitud
				}
			>
				{props.description}
				{user ? (
					<>
						<p className="fw-light text-decoration-none">
							{"Usuario: " +
								user.nombre +
								" " +
								user.apellido_paterno +
								" " +
								user.apellido_materno}

							<br />
							{"Telefono: " + user.celular}
						</p>
					</>
				) : (
					<></>
				)}
			</Toast.Body>
		</Toast>
	);
};

export default ModelToast;
