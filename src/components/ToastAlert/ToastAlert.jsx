import { ToastContainer } from "react-bootstrap";
import ModelToast from "./ModelToast";

const ToastAlert = (props) => {
	/* //console.log(props.arrayalerts); */
	return (
		<ToastContainer
			className="bottom-end"
			style={{ bottom: "25px", right: "15px" }}
		>
			{props.arrayalerts?.map((obj, index) => (
				<ModelToast
					key={"t" + index}
					title={obj.title}
					latitud={obj.latitud}
					longitud={obj.longitud}
					id_usuario={obj.id_usuario}
					description={obj.description}
					onClose={() => props.onClose(index)}
				/>
			)) ?? <></>}
		</ToastContainer>
	);
};

export default ToastAlert;
