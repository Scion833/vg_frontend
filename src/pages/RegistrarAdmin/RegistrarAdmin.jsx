import { Alert, Col, Container, Form, Row } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import { BsPersonCircle } from "react-icons/bs";
import { useRef, useState } from "react";
import { urls_apis, DIRURL } from "../../models/coneccion/comunicacion";
import InputRowComponent from "../../components/InputRowComponent/InputRowComponent";

const RegistrarAdmin = ({ ...props }) => {
	const [showMensaje, setShowMensaje] = useState({
		show: false,
		mensaje: "",
		variant: "danger",
	});

	const refNombres = useRef(null);
	const refApellidoPaterno = useRef(null);
	const refApellidoMaterno = useRef(null);
	const refCI_NIT = useRef(null);
	const refNumeroTelefonico = useRef(null);
	const refCorreoElectronico = useRef(null);
	const refPassword = useRef(null);
	const refVerPassword = useRef(null);

	const btnRegistrar = () => {
		let ver_password = refVerPassword.current.value;
		let password = refPassword.current.value;

		if (password === ver_password) {
			Registrar();
		} else {
			console.log("error en los password");
		}
	};

	const Registrar = () => {
		let nombre = refNombres.current.value();
		let apellido_paterno = refApellidoPaterno.current.value();
		let apellido_materno = refApellidoMaterno.current.value();
		let ci_nit = refCI_NIT.current.value();
		let nro_telefono = refNumeroTelefonico.current.value();
		let correo = refCorreoElectronico.current.value();
		let password = refPassword.current.value;

		let Administrador = {
			nombre,
			apellido_paterno,
			apellido_materno,
			ci_nit,
			nro_telefono,
			correo,
			password,
		};
		console.log(Administrador);
		if (
			refNombres.current.isValid() &&
			refApellidoPaterno.current.isValid() &&
			refApellidoMaterno.current.isValid() &&
			refCI_NIT.current.isValid() &&
			refNumeroTelefonico.current.isValid() &&
			refCorreoElectronico.current.isValid()
		) {
			enviarRegistroAdmin(Administrador);
		}
	};

	const enviarRegistroAdmin = async (Administrador) => {
		await fetch(DIRURL.concat(urls_apis.createAdministrador), {
			method: "POST",
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
				"Content-Type": "application/json",
			},
			body: JSON.stringify(Administrador),
		})
			.then((res) => {
				console.log(res);
				if (res.status === 201) {
					setShowMensaje({
						show: true,
						mensaje: "Se registro correctamente",
						variant: "success",
					});
				} else {
					return res.json();
				}
			})
			.then((data) => {
				console.log(data);
				if (data?.err) {
					switch (data.err?.errno ?? 0) {
						case 19:
							setShowMensaje({
								show: true,
								mensaje: "El correo ya es usado por alguin",
								variant: "danger",
							});

							break;

						default:
							setShowMensaje({
								show: true,
								mensaje:
									"Ocurrio un error al guardar los datos, contacte con sistemas",
								variant: "danger",
							});
							break;
					}
				}
			})
			.catch((err) => {
				console.log(err);
				setShowMensaje({
					show: true,
					mensaje: "Error al comunicarse con el servidor",
					variant: "danger",
				});
			});
	};

	return (
		<Container>
			<Row>
				<Col>
					<center>
						<h1>REGISTRO DE ADMINISTRADOR</h1>
					</center>
				</Col>
			</Row>

			<Row>
				<Col sm={8}>
					<InputRowComponent ref={refNombres} label={"Nombres: "} />
					<InputRowComponent
						ref={refApellidoPaterno}
						label={"Apellido Paterno:"}
					/>
					<InputRowComponent
						ref={refApellidoMaterno}
						label={"Apellido Materno:"}
					/>
					<InputRowComponent ref={refCI_NIT} label={"CI/NIT:"} />
					<InputRowComponent
						ref={refNumeroTelefonico}
						label={"Numero Telefonico:"}
					/>
					<InputRowComponent
						ref={refCorreoElectronico}
						label={"Correo Electronico:"}
						patron={
							/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/
						}
						placeholder={"email@gmail.com"}
					/>
				</Col>

				<Col>
					<Row
						className="text-center"
						style={{ display: "grid", justifyContent: "center" }}
					>
						<Col xs={6} md={4}>
							<BsPersonCircle size={150} />
						</Col>
					</Row>

					<Form.Label htmlFor="inputPassword5">Password</Form.Label>
					<Form.Control
						ref={refPassword}
						type="password"
						id="inputPassword5"
						aria-describedby="passwordHelpBlock"
					/>
					<Form.Text id="passwordHelpBlock" muted>
						Su contraseña debe tener entre 8 y 20 caracteres,
						contener letras y números y no debe contener espacios,
						caracteres especiales ni emoji.
					</Form.Text>

					<Form.Group>
						<Form.Label htmlFor="inputConfirmPassword">
							Confirmar Password
						</Form.Label>
						<Form.Control
							ref={refVerPassword}
							type="password"
							id="inputConfirmPassword"
							aria-describedby="passwordHelpBlock"
							required
						/>
					</Form.Group>

					{showMensaje.show ? (
						<Alert
							variant={showMensaje.variant}
							onClose={() =>
								setShowMensaje({
									show: false,
									mensaje: "",
									variant: "danger",
								})
							}
							dismissible
						>
							<p className="text-center">{showMensaje.mensaje}</p>
						</Alert>
					) : (
						<></>
					)}

					<center>
						<Button variant="primary" onClick={btnRegistrar}>
							REGISTRARSE
						</Button>
					</center>
				</Col>
			</Row>
		</Container>
	);
};

export default RegistrarAdmin;
