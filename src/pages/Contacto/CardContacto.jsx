import { Button, Card, Col, Row } from "react-bootstrap";
import { useState } from "react";

const CardContacto = (props) =>{
    const [nombre, setNombre]=useState(props.nombre);
    const [apellido_paterno, setApellido_paterno]=useState(props.apellido_paterno);
    const [apellido_materno, setApellido_materno]=useState(props.apellido_materno);
    const [telefono, setTelefono]=useState(props.telefono);
    const [direccion, setDireccion]=useState(props.direccion);
    const [parentezco, setParentezco]=useState(props.parentezco);

    const refreshDate=(contacto)=>{
        setNombre(contacto.nombre);
        setApellido_paterno(contacto.apellido_paterno);
        setApellido_materno(contacto.apellido_materno);
        setTelefono(contacto.telefono);
        setDireccion(contacto.direccion);
        setParentezco(contacto.parentezco);
    };

    return(
        <>
            <Table>
					<thead>
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Ap. Paterno</th>
							<th>Ap. Materno</th>
							<th>Telefono</th>
							<th>Direccion</th>
							<th>Parentezo</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody>
						{contacto.map((obj, index) => (
							<tr key={index}>
								<td>{index + 1}</td>
								<td>{obj.nombre}</td>
								<td>{obj.apellido_paterno}</td>
								<td>{obj.apellido_materno}</td>
								<td>{obj.telefono}</td>
								<td>{obj.direccion}</td>
								<td>{obj.parentezco}</td>
								<td>
									<ModalInput
										title={"Editar Contacto"}
										textcancel="Cerrar"
										isactiveacceptbtn={false}
										icononclick={(e) =>{
											return(
												<Button variant="primary" onClick={e}>
													Editar
												</Button>
											)
										}}
									>
										<InputDataContacto
											nombre={obj.nombre}
											apellido_paterno={obj.apellido_paterno}
											apellido_materno={obj.apellido_materno}
											telefono={obj.telefono}
											direccion={obj.direccion}
											parentezco={obj.parentezco}
											id_contacto={obj.id_contacto}
										/>
									</ModalInput>

									<Button variant="danger">Eliminar</Button>
									
									</td>
							</tr>
						))}
					</tbody>
				</Table>
        </>
    );
};

export default CardContacto;