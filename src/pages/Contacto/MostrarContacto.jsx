import { useEffect, useState } from "react";
import { Button, Stack, Table } from "react-bootstrap";
import { DIRURL, urls_apis } from "../../models/coneccion/comunicacion";
import MensajeAlerta from "../Articulo/MensajeAlerta";
import { Spinner } from "react-bootstrap";
import ModeloTitulo from "../TituloPagina/ModeloTitulo";
import ModalInput from "../../components/ModalInput/ModalInput";
import { AiOutlineEdit, AiOutlineUsergroupAdd } from "react-icons/ai";
import InputDataContacto from "./InputDataContacto";
import Contacto from "./Contacto";
import NuevoContacto from "./NuevoContacto";
import { BsPersonAdd } from "react-icons/bs";
import { RiDeleteBin5Fill } from "react-icons/ri";

const TableContactos = (props) => {
	const [contacto, setContacto] = useState([]);
	const [cargando, setCargando] = useState(false);
	let objMensaje = {
		show: false,
		titulo: "",
		mensaje: "",
	};
  	const [mensaje, setMensaje] = useState(objMensaje);
	
	const getAllContactoByUser = async () => {
		setCargando(true);
		setMensaje({
			show: false,
			titulo: "Error al cargar",
			mensaje: "No se pudo obtener los datos",
		});
		await fetch(DIRURL.concat(urls_apis.getAllContactoByUser), {
			method: "GET",
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
				"Content-Type": "application/json",
			}
		})
			.then((res) => {
				if (res.status === 200) {
					return res.json();
				}else{
					setCargando(false);
				}
			})
			.then((data) => {
				setCargando(false);
				setContacto(data?.data??[]);
			})
			.catch((err) => {
				console.log(err)
				setCargando(false);
				let objMensaje = {
					show: true,
					titulo: "Error al cargar",
					mensaje: "No se pudo obtener los datos",
				};
				setMensaje(objMensaje);
			});
	};

	const deleteContacto = async(Contacto)=>{
		setCargando(true);
		setMensaje({
			show: false,
			titulo: "Error al cargar",
			mensaje: "No se pudo obtener los datos",
		});
		await fetch(DIRURL.concat(urls_apis.deleteContacto),{
			method : "delete",
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
				"Content-Type": "application/json",
			},
			body: JSON.stringify(Contacto),
		})
		.then((res) => {
			if (res.status === 200) {
				return res.json();
			}else{
				setCargando(false);
			}
		})
		.then((data) => {
			setCargando(false);
			setContacto(data?.data??[]);
		})
		.catch((err) => {
			console.log(err)
			setCargando(false);
			let objMensaje = {
				show: true,
				titulo: "Error al cargar",
				mensaje: "No se pudo obtener los datos",
			};
			setMensaje(objMensaje);
		});
	};
	

	useEffect(() => {
		getAllContactoByUser();
		deleteContacto();
		// eslint-disable-next-line
	}, []);

	

	return (
		<div className="container">	
			<ModeloTitulo titulo={"Mis Contactos"} />
			<div className="row row-cols-1 row-cols-md-3 g-5 mb-5">
				{cargando ? <Spinner animation="grow" /> : <></>}
				<MensajeAlerta
					show={mensaje.show}
					titulo={mensaje.titulo}
					mensaje={mensaje.mensaje}
					onClick={getAllContactoByUser}
				/>

				<Table>
					<thead>
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Ap. Paterno</th>
							<th>Ap. Materno</th>
							<th>Telefono</th>
							<th>Direccion</th>
							<th>Parentezo</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody>
						{contacto.map((obj, index) => (
							<tr key={index}>
								<td>{index + 1}</td>
								<td>{obj.nombre}</td>
								<td>{obj.apellido_paterno}</td>
								<td>{obj.apellido_materno}</td>
								<td>{obj.telefono}</td>
								<td>{obj.direccion}</td>
								<td>{obj.parentezco}</td>
								<td>
									<ModalInput
										title={"Editar Contacto"}
										textcancel="Cerrar"
										isactiveacceptbtn={false}
										icononclick={(e) =>{
											return(
												<Button variant="outline-primary" onClick={e}>
													<AiOutlineEdit size={"15px"}/>
													Editar
												</Button>
											)
										}}
									>
										<InputDataContacto
											nombre={obj.nombre}
											apellido_paterno={obj.apellido_paterno}
											apellido_materno={obj.apellido_materno}
											telefono={obj.telefono}
											direccion={obj.direccion}
											parentezco={obj.parentezco}
											id_contacto={obj.id_contacto}
										/>
									</ModalInput>

									<Button variant="outline-danger">
										<RiDeleteBin5Fill size={"15px"}/>
										Eliminar
									</Button>
									
									</td>
							</tr>
						))}
					</tbody>
				</Table>
				<ModalInput
					title={"Nuevo Contacto"}
					textcancel="Cerrar"
					isactiveacceptbtn={false}
					icononclick={(n) =>{
						return(
							<Button style={{width:230, marginTop:30,}} variant="success" onClick={n}>
								<BsPersonAdd size={"30px"} />
								Nuevo Contacto
							</Button>
						)
					}}
				>
					<NuevoContacto/>
				</ModalInput>
			</div>
			
		</div>
	);
};

export default TableContactos;
