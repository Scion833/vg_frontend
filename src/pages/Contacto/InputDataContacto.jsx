import { useRef, useState } from "react";
import { Alert, Button, Container, Spinner, Stack } from "react-bootstrap";
import InputRowComponent from "../../components/InputRowComponent/InputRowComponent";
import { DIRURL, urls_apis } from "../../models/coneccion/comunicacion";

const InputDataContacto=(props)=>{
    const [isLoading, setIsLoading] = useState(false);
	const [messageStatus, setMessageStatus] = useState({
		title: "",
		message: "",
		isShow: false,
		variant: "",
	});

    const refNombre = useRef(null);
	const refApellidoPaterno = useRef(null);
	const refApellidoMaterno = useRef(null);
	const refTelefono = useRef(null);
	const refDireccion = useRef(null);
	const refParentezco = useRef(null);

    const objectContacto={
        nombre:"",
        apellido_paterno:"",
        apellido_materno:"",
        telefono:"",
        direccion:"",
        parentezco:"",

    };

	// eslint-disable-next-line
    const [nombre, setNombre]=useState(props.nombre);
	// eslint-disable-next-line
    const [apellido_paterno, setApellido_paterno]=useState(props.apellido_paterno);
	// eslint-disable-next-line
    const [apellido_materno, setApellido_materno]=useState(props.apellido_materno);
	// eslint-disable-next-line
    const [telefono, setTelefono]=useState(props.telefono);
	// eslint-disable-next-line
    const [direccion, setDireccion]=useState(props.direccion);
	// eslint-disable-next-line
    const [parentezco, setParentezco]=useState(props.parentezco);

    const refreshDate=(contacto)=>{
        setNombre(contacto.nombre);
        setApellido_paterno(contacto.apellido_paterno);
        setApellido_materno(contacto.apellido_materno);
        setTelefono(contacto.telefono);
        setDireccion(contacto.direccion);
        setParentezco(contacto.parentezco);
    };

    const saveContacto = async()=>{
        if(
            refNombre.current.isValid() &&
            refApellidoPaterno.current.isValid() &&
            refApellidoMaterno.current.isValid() &&
            refTelefono.current.isValid() &&
            refDireccion.current.isValid() &&
            refParentezco.current.isValid()
        ){
            setIsLoading(true);

            objectContacto.nombre = refNombre.current.value();
            objectContacto.apellido_paterno = refApellidoPaterno.current.value();
            objectContacto.apellido_materno = refApellidoMaterno.current.value();
            objectContacto.telefono = refTelefono.current.value();
            objectContacto.direccion = refDireccion.current.value();
            objectContacto.parentezco = refParentezco.current.value();
            objectContacto.id_contacto = props.id_contacto;

            await fetch(DIRURL.concat(urls_apis.updateContacto),{
                method: "PUT",
                headers:{
                    "x-token": localStorage.getItem("x-token"),
					Accept: "*/*",
					"Content-Type": "application/json",
                },
                body: JSON.stringify(objectContacto),
            })
            .then((res) => {
                switch (res.status) {
                    case 200:
                        setMessageStatus({
                            title: "Guardado!!",
                            message: "Se guardo correctamente",
                            isShow: true,
                            variant: "success",
                        });
                        refreshDate(objectContacto);
                        return res.json();
                    case 409:
                        setMessageStatus({
                            title: "Error 409",
                            message: "Error al guardar los datos",
                            isShow: true,
                            variant: "danger",
                        });
                        break;
                    case 401:
                        setMessageStatus({
                            title: "Error 401",
                            message: "Usuario no autorizado",
                            isShow: true,
                            variant: "danger",
                        });
                        break;
                    default:
                        break;
                }
            })
            .then((data) => {
                console.log(data);
            })
            .catch((err) => {
                setMessageStatus({
                    title: "Error",
                    message: "Se produjo un error al enviar los datos",
                    isShow: true,
                    variant: "danger",
                });
                console.log(err);
            })
            .finally(() => {
                setIsLoading(false);
            });
        }else{
            console.log("No soy valido");
        }
    };
    return(
        <Container>
            <InputRowComponent
                value={props.nombre}
                ref={refNombre}
                label={"Nombre"}
            />

            <InputRowComponent
                value={props.apellido_paterno}
                ref={refApellidoPaterno}
                label={"Apellido Paterno"}
            />

            <InputRowComponent
                value={props.apellido_materno}
                ref={refApellidoMaterno}
                label={"Apellido Materno"}
            />
            
            <InputRowComponent
                value={props.telefono}
                ref={refTelefono}
                label={"Telefono"}
            />

            <InputRowComponent
                value={props.direccion}
                ref={refDireccion}
                label={"Direccion"}
            />

            <InputRowComponent
                value={props.parentezco}
                ref={refParentezco}
                label={"Parentezco"}
            />

            {messageStatus.isShow ? (
				<Alert
					className="mt-4"
					variant={messageStatus.variant}
					onClose={() => {
						setMessageStatus({
							title: "",
							message: "",
							isShow: false,
							variant: "",
						});
					}}
					dismissible
				>
					<Alert.Heading>{messageStatus.title}</Alert.Heading>
					<p>{messageStatus.message}</p>
				</Alert>
			) : (
				<></>
			)}
			<Stack gap={2} className="col-md-5 mx-auto mt-4">
				<Button
					variant="primary"
					onClick={saveContacto}
					disabled={isLoading}
				>
					{isLoading ? <Spinner animation="grow" /> : <></>}
					Guardar
				</Button>
			</Stack>

        </Container>
    );
};


export default InputDataContacto;