import { Col, Container, Row } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import { useRef, useState } from "react";
import { urls_apis, DIRURL } from "../../models/coneccion/comunicacion";
import InputRowComponent from "../../components/InputRowComponent/InputRowComponent";

const Contacto = ({ ...props }) => {
	const [showMensaje, setShowMensaje] = useState({
		show: false,
		mensaje: "",
		variant: "danger",
	});

	const refNombre = useRef(null);
	const refApellidoPaterno = useRef(null);
	const refApellidoMaterno = useRef(null);
	const refTelefono = useRef(null);
	const refDireccion = useRef(null);
	const refParentezco = useRef(null);

	const btnRegistrar = () => {
		Registrar();
	};

	const Registrar = () => {
		let nombre = refNombre.current.value();
		let apellido_paterno = refApellidoPaterno.current.value();
		let apellido_materno = refApellidoMaterno.current.value();
		let telefono = refTelefono.current.value();
		let direccion = refDireccion.current.value();
		let parentezco = refParentezco.current.value();

		let Contacto = {
			nombre,
			apellido_paterno,
			apellido_materno,
			telefono,
			direccion,
			parentezco,
		};
		console.log(Contacto);

		/*if(     refNombre.current.isValid() &&
            refApellidoPaterno.current.isValid() &&
            refApellidoMaterno.current.isValid() &&
            refTelefono.current.isValid() &&
            refDireccion.current.isValid() &&
            refParentezco.current.isValid()){
              
		  enviarRegistroContacto(Contacto);
	  
    }*/
		enviarRegistroContacto(Contacto);
	};
	const enviarRegistroContacto = async (Contacto) => {
		await fetch(DIRURL.concat(urls_apis.createContacto), {
			method: "POST",
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
				"Content-Type": "application/json",
			},
			body: JSON.stringify(Contacto),
		})
			.then((res) => {
				console.log(res);
				if (res.status === 201) {
					setShowMensaje({
						show: true,
						mensaje: "Se registro correctamente",
						variant: "success",
					});
				} else {
					return res.json();
				}
			}) /*.then(data=>{
          console.log(data)
          if(data?.err){
            switch (data.err?.errno ?? 0) {
              case 19:
                setShowMensaje({show:true,mensaje:'El correo ya es usado por alguin',variant:'danger'});
                
                break;
            
              default:
                setShowMensaje({show:true,mensaje:'Ocurrio un error al guardar los datos, contacte con sistemas',variant:'danger'});
                break;
            }
          }
        })*/
			.catch((err) => {
				console.log(err);
				setShowMensaje({
					show: true,
					mensaje: "Error al comunicarse con el servidor",
					variant: "danger",
				});
			});
	};

	return (
		<Container>
			<Row>
				<Col>
					<center>
						<h1>REGISTRO DE CONTACTO</h1>
					</center>
				</Col>
			</Row>
			{showMensaje?<></>:<></>}

			<Row>
				<Col sm={8}>
					<InputRowComponent ref={refNombre} label={"Nombres: "} />
					<InputRowComponent
						ref={refApellidoPaterno}
						label={"Apellido Paterno:"}
					/>
					<InputRowComponent
						ref={refApellidoMaterno}
						label={"Apellido Materno:"}
					/>
					<InputRowComponent
						ref={refDireccion}
						label={"Direccion:"}
					/>
					<InputRowComponent
						ref={refTelefono}
						label={"Numero Telefonico:"}
					/>
					<InputRowComponent
						ref={refParentezco}
						label={"Parentezco: "}
					/>
				</Col>

				<center>
					<Button variant="primary" onClick={btnRegistrar}>
						REGISTRAR
					</Button>
				</center>
			</Row>
		</Container>
	);
};
export default Contacto;

/*const btnRegistrar = () => {
      	
      	let ver_password = refVerPassword.current.value;
    	  let password = refPassword.current.value;

      	if (password === ver_password) {
        	Registrar();
      	} else {
        	console.log("error en los password");
      	}
    };
  
    const Registrar = () => {
		let nombre = refNombres.current.value();
		let apellido_paterno = refApellidoPaterno.current.value();
		let apellido_materno = refApellidoMaterno.current.value();
		let direccion = refDireccion.current.value();
		let nro_telefono = refNumeroTelefonico.current.value();
		let parentezco = Parentezco.current.value();
		
      
  
      let Contacto = {
        nombre,
        apellido_paterno,
        apellido_materno,
        direccion: direccion,
        nro_telefono,
        parentezco: parentezco,
      };
      console.log(Contacto);
	  if(refNombres.current.isValid() &&
	     refApellidoPaterno.current.isValid() &&
		 refApellidoMaterno.current.isValid() &&
		 refDireccion.current.isValid() &&
		 refNumeroTelefonico.current.isValid() &&
		 Parentezco.current.isValid()){
		  
	  }
    };*/
