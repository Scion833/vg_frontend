import { Alert, Col, Container, Row, Spinner, Stack } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import { useRef, useState } from "react";
import { DIRURL, urls_apis } from "../../models/coneccion/comunicacion";
import InputRowComponent from "../../components/InputRowComponent/InputRowComponent";


const NuevoContacto=({props}) =>{
    const [isLoading, setIsLoading] = useState(false);
	const [messageStatus, setMessageStatus] = useState({
		title: "",
		message: "",
		isShow: false,
		variant: "",
	});

    const refNombre = useRef(null);
    const refApellidoPaterno = useRef(null);
    const refApellidoMaterno = useRef(null);
    const refTelefono = useRef(null);
    const refDireccion = useRef(null);
    const refParentezco = useRef(null);

    const btnRegistrar = () => {
        Registrar();
    };

    const Registrar = () => {
		let nombre = refNombre.current.value();
		let apellido_paterno = refApellidoPaterno.current.value();
		let apellido_materno = refApellidoMaterno.current.value();
		let telefono = refTelefono.current.value();
		let direccion = refDireccion.current.value();
		let parentezco = refParentezco.current.value();

		let Contacto = {
			nombre,
			apellido_paterno,
			apellido_materno,
			telefono,
			direccion,
			parentezco,
		};
		console.log(Contacto);

        enviarRegistroContacto(Contacto);
    };

    const enviarRegistroContacto = async (Contacto) => {
		await fetch(DIRURL.concat(urls_apis.createContacto), {
			method: "POST",
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
				"Content-Type": "application/json",
			},
			body: JSON.stringify(Contacto),
		})
        .then((res)=>{
            console.log(res);
            switch (res.status){
                case 200:
                    setMessageStatus({
                        title: "Guardado!!",
                        message: "Se guardo correctamente",
                        isShow: true,
                        variant: "success",
                    });
                    return res.json();
                case 406:
                    setMessageStatus({
                        title: "Error 406",
                        message: "Error al guardar los datos",
                        isShow: true,
                        variant: "danger",
                    });
                    break;
                default:
                    break;
            }
        })
        .then((data)=>{
            console.log(data);
        })
        .catch((err)=>{
            setMessageStatus({
                title: "Error",
                message: "Se produjo un error al enviar los datos",
                isShow: true,
                variant: "danger",
            });
            console.log(err);
        })
        .finally(()=>{
            setIsLoading(false);
        });
    };
    return(
        <Container>
            <Row>
                <Col>
                    <center>
                        <h1>NUEVO CONTACTO</h1>
                    </center>
                </Col>
            </Row>
            <Row>
                <InputRowComponent
                    ref={refNombre}
                    label={"Nombre"}
                />

                <InputRowComponent
                    ref={refApellidoPaterno}
                    label={"Apellido Paterno"}
                />

                <InputRowComponent
                    ref={refApellidoMaterno}
                    label={"Apellido Materno"}
                />
                
                <InputRowComponent
                    ref={refTelefono}
                    label={"Telefono"}
                />

                <InputRowComponent
                    ref={refDireccion}
                    label={"Direccion"}
                />

                <InputRowComponent
                    ref={refParentezco}
                    label={"Parentezco"}
                />

{messageStatus.isShow ? (
				<Alert
					className="mt-4"
					variant={messageStatus.variant}
					onClose={() => {
						setMessageStatus({
							title: "",
							message: "",
							isShow: false,
							variant: "",
						});
					}}
					dismissible
				>
				<Alert.Heading>{messageStatus.title}</Alert.Heading>
					<p>{messageStatus.message}</p>
				</Alert>
			) : (
				<></>
			)}
			<Stack gap={2} className="col-md-5 mx-auto mt-4">
				<Button
					variant="success"
					onClick={btnRegistrar}
					disabled={isLoading}
				>
					{isLoading ? <Spinner animation="grow" /> : <></>}
					Guardar
				</Button>
			</Stack>
            </Row>
            
        </Container>
    );
};

export default NuevoContacto;

