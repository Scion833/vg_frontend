import { Container, Table } from "react-bootstrap";
import { DIRURL, urls_apis } from "../../models/coneccion/comunicacion";
import { useEffect, useState } from "react";

const Usuarios = ({ ...props }) => {
	const [usuarios, setUsuarios] = useState([]);
	const getUsers = async () => {
		await fetch(DIRURL.concat(urls_apis.getAllUsuarios), {
			method: "GET",
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
				"Content-Type": "application/json",
			},
		})
			.then((res) => {
				switch (res.status) {
					case 200:
						return res.json();

					default:
						break;
				}
			})
			.then((data) => {
				setUsuarios(data.data);
			})
			.catch((err) => {
				console.log(err);
			})
			.finally(() => {
				console.log("finalizo");
			});
	};

	useEffect(() => {
		getUsers();
	}, []);

	return (
		<Container>
			<div>
				<h2 className="text-center text-uppercase mt-3">
					Gestion Usuarios y alertas
				</h2>
			</div>
			<Table className="mt-4" striped bordered hover size="sm" responsive>
				<thead>
					<tr>
						<th>#</th>
						<th>Nombre</th>
						<th>Apellido Paterno</th>
						<th>Apellido Materno</th>
						<th>Genero</th>
						<th>Ciudad</th>
						<th>Celular</th>
					</tr>
				</thead>
				<tbody>
					{usuarios.map((obj, index) => (
						<tr key={obj.id_usuario}>
							<td>{index+1}</td>
							<td>{obj.nombre}</td>
							<td>{obj.apellido_paterno}</td>
							<td>{obj.apellido_materno}</td>
							<td>{obj.genero}</td>
							<td>{obj.ciudad}</td>
							<td>{obj.celular}</td>
						</tr>
					))}
				</tbody>
			</Table>
		</Container>
	);
};

export default Usuarios;
