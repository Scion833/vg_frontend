const ModeloTitulo = (props) => {
    return (
    <div className="row my-4 justify-content-center" style={{ fontFamily: "Ginebra", color:"#6E93BB" }}>
        <div className="col-9">
            <div className="display-1 fw-semibold text-center">
                {props.titulo}
            </div>
        </div>
    </div>
  );
};

export default ModeloTitulo;