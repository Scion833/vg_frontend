import { BsFillPencilFill, BsPencilSquare } from "react-icons/bs";
import { DIRURL, urls_apis } from "../../../models/coneccion/comunicacion";
import { useRef } from "react";

const RegistroChatBot = ({ ...props }) => {
  const refPregunta = useRef(null);
  const refRespuesta = useRef(null);

  const PostChatBot = async () => {
    let headersList = {
      Accept: "*/*",
      "x-token": localStorage.getItem("x-token"),
      "Content-Type": "application/json",
    };

    await fetch(DIRURL.concat(urls_apis.AddChatBot), {
      method: "POST",
      //poner los nombres identicos a los atributos de la tabla
      body: JSON.stringify({
        pregunta: refPregunta.current.value,
        respuesta: refRespuesta.current.value,
      }),
      headers: headersList,
    })
      .then((res) => res.json())
      .then((data) => console.log(data))
      .catch((err) => console.log(err));
  };

  return (
    <div class="container">
      <div class="row my-5 justify-content-center">
        <div class="col-12 col-md-9">
          <h1 class="display-2 fw-bold text-center text-info">
            Agregar Preguntas Y Respuestas
          </h1>
        </div>
      </div>

      <div class="row justify-content-center">
        <div class="col-12 col-sm-11 col-md-8 col-lg-9 col-xl-7 col-xxl-6">
          <div class="shadow-lg border border-secondary-subtle mb-5">
            <form
              onSubmit={(e) => e.preventDefault()}
              class="was-validated my-4 mx-4"
            >
              <div class="row mb-4">
                <div class="col-12 col-lg-3 col-xl-3 col-xxl-3">
                  <label
                    for="inputPregunta"
                    class="col-form-label col-form-label-lg"
                  >
                    Pregunta
                    <BsFillPencilFill class="ms-2 mb-2" size={23} />
                  </label>
                </div>
                <div class="col-12 col-sm-12 col-lg-9 col-xl-9 col-xxl-9">
                  <input
                    type="text"
                    class="form-control form-control-lg"
                    ref={refPregunta}
                    placeholder="Agregar Pregunta"
                    required
                  />
                </div>
              </div>

              <div class="row mb-4">
                <div class="col-12 col-sm-12">
                  <div class="input-group input-group-lg">
                    <label class="input-group-text" for="inputRespuesta">
                      Registrar Respuesta
                      <BsPencilSquare class="ms-2" size={23} />
                    </label>
                    <textarea
                      class="form-control"
                      ref={refRespuesta}
                      rows="4"
                      aria-label="Notas"
                      required
                    ></textarea>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-12 d-flex justify-content-start">
                  <button
                    class="btn btn-lg fs-2 text-white"
                    style={{
                      backgroundColor: "steelblue",
                    }}
                    onClick={() => {
                      PostChatBot();
                    }}
                  >
                    Registrar
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RegistroChatBot;
