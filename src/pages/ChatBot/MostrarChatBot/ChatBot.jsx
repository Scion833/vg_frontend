import { useEffect, useState } from "react";
import ModeloTitulo from "../../TituloPagina/ModeloTitulo";
import ModeloChatBot from "./ModeloChatBot";
import { DIRURL, urls_apis } from "../../../models/coneccion/comunicacion";

const ChatBot = (props) => {

    const [arrayChatBot,setArrayChatBot]=useState([]);

    const getChatBot=async () =>{
    let headersList = {
        "Accept": "*/*",
        "User-Agent": "Thunder Client (https://www.thunderclient.com)"
       }
       
       await fetch(DIRURL.concat(urls_apis.GetAllChatBot), { 
         method: "GET",
         headers: headersList
       }).then(res=>res.json()).then(data=>{
        setArrayChatBot(data.data)
       }).catch(err=>console.log(err))
       
    }

    useEffect(()=>{
        getChatBot()
    },[])


	return (
        <div className="container">
			<ModeloTitulo titulo={"Preguntas Frecuentes"} />

            {arrayChatBot.map((ch,index)=>{
                return(
                    <ModeloChatBot
                    key={ch.id_bpr}
                    pregunta={ch.pregunta}  
                    respuesta={ch.respuesta}                    
                    />
                )
            })}
		</div>
	);
};

export default ChatBot;
