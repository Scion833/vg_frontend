const ModeloChatBot = (props) => {

	return (
        <div className="row mb-5 justify-content-center">
                    
                    <div className="col-12 col-md-8">
            
                        <div className="shadow-lg">
                
                            <div className="card">
                
                                <div className="card">
                                    
                                    <div className="card-header fs-3 fw-bold text-danger">
                                        {props.pregunta}
                                    </div>

                                    <div className="card-body">

                                            <div className="card-text text-secondary">
                                                <div className="overflow-auto fs-5" style={{maxHeight: "180px"}}>
                                                    <p className="mb-0 me-1">
                                                      {props.respuesta}
                                                     </p>
                                                 </div>
                                            </div>
                                    </div>

                                </div>
            
                            </div> 

                        </div>

                    </div>

        </div>
	);
};

export default ModeloChatBot;
