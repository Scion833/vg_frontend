import { Alert, Col, Container, Form, Row } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import { BsPersonCircle } from "react-icons/bs";
import { useEffect, useRef, useState } from "react";
import { headers, urls_apis, DIRURL } from "../../models/coneccion/comunicacion";
import InputRowComponent from "../../components/InputRowComponent/InputRowComponent";
import { useNavigate } from "react-router-dom";

const Registrar = ({ ...props }) => {
	const [mensajeError, setMensajeError] = useState(false);
	const [passwordValidate, setPasswordValidate] = useState(false);
	const [arrayIdentidad, setArrayIdentidad] = useState([]);

	const refNombres = useRef(null);
	const refApellidoPaterno = useRef(null);
	const refApellidoMaterno = useRef(null);
	const refCI = useRef(null);
	const refExtension = useRef(null); //
	const refFechadeNacimiento = useRef(null);

	const refGenero = useRef(null);
	const refIdentidad = useRef(null);

	const refNumeroTelefonico = useRef(null);
	const refCorreoElectronico = useRef(null);
	const refCiudad = useRef(null);
	const refDireccion = useRef(null);
	const refEstadoCivil = useRef(null); //
	const refProfesion = useRef(null);
	const refPassword = useRef(null);
	const refVerPassword = useRef(null);

	const navigate = useNavigate();

	const btnRegistrar = () => {
		let ver_password = refVerPassword.current.value;
		let password = refPassword.current.value;
		setMensajeError(password !== ver_password);
		let patron = /^[a-zA-Z0-9]{8,20}$/;
		setPasswordValidate(!patron.test(password));

		if (password === ver_password && patron.test(password)) {
			Registrar();
		} else {
			console.log("error en los password");
		}
	};

	const Registrar = () => {
		let nombre = refNombres.current.value();
		let apellido_paterno = refApellidoPaterno.current.value();
		let apellido_materno = refApellidoMaterno.current.value();
		let ci = parseInt(refCI.current.value());
		let fecha_nacimiento = refFechadeNacimiento.current.value();

		let genero = parseInt(refGenero.current.value);
		let id_identidad = parseInt(refIdentidad.current.value);

		let celular = refNumeroTelefonico.current.value();
		let correo = refCorreoElectronico.current.value();
		let ciudad = refCiudad.current.value();
		let direccion = refDireccion.current.value();
		let estado_civil = refEstadoCivil.current.value;
		let profesion = refProfesion.current.value();
		let password = refPassword.current.value;
		let extencion = refExtension.current.value;

		let usuario = {
			nombre,
			apellido_paterno,
			apellido_materno,
			ci,
			fecha_nacimiento,
			genero,
			id_identidad,
			celular,
			correo,
			ciudad,
			direccion,
			estado_civil,
			profesion,
			password,
			extencion,
		};
		if (
			refNombres.current.isValid() &&
			refApellidoPaterno.current.isValid() &&
			refApellidoMaterno.current.isValid() &&
			refCI.current.isValid() &&
			refFechadeNacimiento.current.isValid() &&
			refNumeroTelefonico.current.isValid() &&
			refCorreoElectronico.current.isValid() &&
			refCiudad.current.isValid() &&
			refDireccion.current.isValid() &&
			refProfesion.current.isValid()
		) {
			enviarRegistro(usuario);
		}
	};

	const enviarRegistro = async (usuario) => {
		await fetch(DIRURL.concat(urls_apis.registrar), {
			method: "POST",
			headers,
			body: JSON.stringify(usuario),
		})
			.then((res) => {
				console.log(res);
				navigate("/login");
			})
			.catch((err) => {
				console.log(err);
			});
	};

	const getAllIndentidad = async () => {
		await fetch(DIRURL.concat(urls_apis.getAllIndentidad), {
			method: "GET",
			headers,
		})
			.then((res) => res.json())
			.then((data) => {
				setArrayIdentidad(data.data);
			})
			.catch((err) => console.log(err));
	};

	useEffect(() => {
		getAllIndentidad();
	}, []);

	return (
		<Container>
			<Row>
				<Col>
					<center>
						<h1>REGISTRO DE USUARIOS</h1>
					</center>
				</Col>
			</Row>
			<Row>
				<Col sm={8}>
					<InputRowComponent ref={refNombres} label={"Nombres"} />

					<InputRowComponent
						ref={refApellidoPaterno}
						label={"Apellido Paterno"}
					/>

					<InputRowComponent
						ref={refApellidoMaterno}
						label={"Apellido Materno:"}
					/>
					<Row>
						<Col>
							<InputRowComponent
								patron={/^[0-9]+$/}
								ref={refCI}
								label={"CI:"}
							/>
						</Col>
						<Col>
							<Form.Select
								ref={refExtension}
								aria-label="Default select example"
							>
								<option value="0">Extension</option>
								<option value="LP">LP</option>
								<option value="OR">OR</option>
								<option value="PT">PT</option>
								<option value="CB">CB</option>
								<option value="SC">SC</option>
								<option value="BN">BN</option>
								<option value="PA">PA</option>
								<option value="TJ">TJ</option>
								<option value="CH">CH</option>
							</Form.Select>
						</Col>
					</Row>

					<InputRowComponent
						ref={refFechadeNacimiento}
						label={"Fecha de Nacimiento:"}
						type="date"
					/>

					<Row>
						<Col>
							<Form.Select
								ref={refGenero}
								aria-label="Default select example"
							>
								<option>Genero</option>
								<option value="1">Masculino</option>
								<option value="0">Femenino</option>
							</Form.Select>
						</Col>
					</Row>

					<Row>
						<Col>
							<Form.Select
								ref={refIdentidad}
								aria-label="Default select example"
							>
								<option value={0}>Identidad</option>
								{arrayIdentidad.map((obj, index) => (
									<option
										key={"identidad" + index}
										value={obj.id_identidad}
									>
										{obj.sexo}
									</option>
								))}
							</Form.Select>
						</Col>
					</Row>

					<InputRowComponent
						ref={refNumeroTelefonico}
						label={"Número Telefonico:"}
						patron={/^[0-9]+$/}
					/>

					<InputRowComponent
						ref={refCorreoElectronico}
						label={"Correo Electronico:"}
						patron={
							/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/
						}
						placeholder={"email@gmail.com"}
					/>

					<InputRowComponent ref={refCiudad} label={"Ciudad:"} />

					<InputRowComponent
						ref={refDireccion}
						label={"Direccion:"}
					/>

					<Row>
						<Col>
							<Form.Select
								ref={refEstadoCivil}
								aria-label="Default select example"
							>
								<option>Estado Civil</option>
								<option value="Soltero(a)">Soltero(a)</option>
								<option value="Casado(a)">Casado(a)</option>
								<option value="Viudo(a)">Viudo(a)</option>
							</Form.Select>
						</Col>
					</Row>

					<InputRowComponent
						ref={refProfesion}
						label={"Profesion:"}
					/>
				</Col>

				<Col>
					<Row
						className="text-center"
						style={{ display: "grid", justifyContent: "center" }}
					>
						<Col xs={6} md={4}>
							<BsPersonCircle size={150} />
						</Col>
					</Row>

					<Form.Label htmlFor="inputPassword5">Password</Form.Label>
					<Form.Control
						ref={refPassword}
						type="password"
						id="inputPassword5"
						aria-describedby="passwordHelpBlock"
						isInvalid={passwordValidate}
					/>
					<Form.Text id="passwordHelpBlock" muted>
						Su contraseña debe tener entre 8 y 20 caracteres,
						contener letras y números y no debe contener espacios,
						caracteres especiales ni emoji.
					</Form.Text>

					<Form.Group>
						<Form.Label htmlFor="inputConfirmPassword">
							Confirmar Password
						</Form.Label>
						<Form.Control
							ref={refVerPassword}
							type="password"
							id="inputConfirmPassword"
							aria-describedby="passwordHelpBlock"
							required
						/>
					</Form.Group>

					{mensajeError ? (
						<Alert
							variant="danger"
							onClose={() => setMensajeError(false)}
							dismissible
						>
							<p>Las contraseñas no coinciden</p>
						</Alert>
					) : (
						<></>
					)}

					<center>
						<Button variant="primary" onClick={btnRegistrar}>
							REGISTRARSE
						</Button>
					</center>
				</Col>
			</Row>
		</Container>
	);
};

export default Registrar;
