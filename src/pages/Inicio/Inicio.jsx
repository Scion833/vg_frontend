import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import "atropos/css";
import Atropos from "atropos/react";
import imagen1 from "./imagen1.jpg";
import imagen2 from "./imagen2.jpg";
import imagen3 from "./imagen3.png";
import imagen4 from "./imagen4.jpg";

const titleStyle = {
  fontFamily: 'Times New Roman', // Cambio de tipo de letra
  color: '#007BFF', // Cambio de color (tono azul oscuro)
  textTransform: 'uppercase', // Titulos en mayúsculas
  fontSize: '2rem', // Tamaño de fuente más grande
  marginBottom: '20px', // Mayor separación inferior
};

const Inicio = () => {
  return (
    <Container>
      {/* Sección Misión */}
      <Row className="justify-content-md-center text-center">
        <Col>
          <div className="text-start p-2">
            <h2 style={titleStyle}>Misión</h2>
            <p>
              Nuestra misión es combatir la violencia de género y promover la igualdad de género a través de una plataforma en línea segura y accesible. Buscamos proporcionar recursos informativos, apoyo emocional y un espacio para denunciar casos de violencia de género, todo ello con el objetivo de empoderar a las víctimas y fomentar un cambio social hacia una sociedad más justa y libre de violencia de género.
            </p>
          </div>
        </Col>
        <Col>
          <Atropos
            className="my-atropos"
            style={{ width: "510px", height: "310px" }}
          >
            <img
              alt="Misión"
              src={imagen1}
              data-atropos-offset="5"
              style={{ width: "500px", height: "300px" }}
            />
          </Atropos>
        </Col>
      </Row>

      {/* Sección Visión */}
      <Row className="justify-content-md-center text-center">
        <Col>
          <Atropos
            className="my-atropos"
            style={{ width: "510px", height: "310px" }}
          >
            <img
              alt="Visión"
              src={imagen2}
              data-atropos-offset="5"
              style={{ width: "500px", height: "300px" }}
            />
          </Atropos>
        </Col>
        <Col>
          <div className="text-start p-2">
            <h2 style={titleStyle}>Visión</h2>
            <p>
              Nuestra visión es crear un mundo donde todas las personas, sin importar su género, puedan vivir libres de violencia y discriminación. Imaginamos una comunidad en línea donde las víctimas se sientan empoderadas para buscar ayuda, denunciar casos y compartir sus historias, contribuyendo así a la concienciación y la prevención de la violencia de género en todo el mundo.
            </p>
          </div>
        </Col>
      </Row>

      {/* Sección Objetivos */}
      <Row>
        <Col>
          <div className="text-start p-2">
            <h2 style={titleStyle}>Objetivos</h2>
            <ul>
              <li>Informar y Educar: Proporcionar información precisa y recursos educativos sobre la violencia de género, sus formas, consecuencias y cómo prevenirla.</li>
              <li>Ofrecer Apoyo: Brindar un entorno de apoyo emocional y psicológico a las víctimas de violencia de género, conectándolas con profesionales y organizaciones de ayuda.</li>
              <li>Facilitar Denuncias: Permitir a las víctimas denunciar casos de violencia de género de forma segura y anónima, proporcionando información sobre recursos legales y procesos judiciales.</li>
              <li>Conectar con Recursos Locales: Mostrar un mapa interactivo con ubicaciones de módulos policiales, centros médicos y centros de ayuda para que las víctimas puedan acceder a servicios cercanos.</li>
              <li>Compartir Historias: Permitir a los usuarios compartir sus experiencias, ya sea a través de textos, fotografías, videos o audios, para crear conciencia y solidaridad.</li>
              <li>Fomentar la Sensibilización: Organizar campañas de sensibilización y eventos en línea para promover la igualdad de género y la prevención de la violencia.</li>
              <li>Proteger la Privacidad: Garantizar la confidencialidad y seguridad de los datos y la información compartida por los usuarios.</li>
              <li>Colaborar con Organizaciones: Establecer alianzas estratégicas con organizaciones gubernamentales y no gubernamentales dedicadas a la lucha contra la violencia de género.</li>
            </ul>
          </div>
        </Col>
        <Col>
          <Atropos
            className="my-atropos"
            style={{ width: "510px", height: "410px" }}
          >
            <img
              alt="Objetivos"
              src={imagen3}
              data-atropos-offset="5"
              style={{ width: "500px", height: "400px" }}
            />
          </Atropos>
        </Col>
      </Row>

      {/* Sección Quiénes Somos */}
      <Row className="justify-content-md-center text-center">
        <Atropos
          className="my-atropos"
          style={{ width: "900px", height: "220px" }}
        >
          <img
            alt="Quiénes Somos"
            src={imagen4}
            data-atropos-offset="5"
            style={{ width: "890px", height: "220px" }}
          />
        </Atropos>

        
          <div className="text-start p-2">
            <h2 style={titleStyle}>Quiénes Somos</h2>
            <p>
              Somos un equipo apasionado de profesionales y defensores de los derechos humanos comprometidos en la lucha contra la violencia de género. Nuestra diversidad en experiencias y habilidades nos une en la misión de crear un mundo más seguro y equitativo para todas las personas, independientemente de su género.

              Nos esforzamos por brindar una plataforma en línea segura y comprensiva que sea un recurso invaluable para las víctimas de violencia de género y aquellos que buscan un cambio positivo en la sociedad. Nuestra dedicación y empatía nos impulsan a trabajar incansablemente para lograr un mundo libre de violencia de género.

              Creemos en la fuerza de la comunidad y en el poder de la información y la educación para crear un cambio duradero. Trabajamos en colaboración con organizaciones y expertos en el campo de la violencia de género para asegurarnos de que nuestra plataforma ofrezca la mejor asistencia posible.

              En última instancia, estamos aquí para escuchar, apoyar y empoderar a todas las personas afectadas por la violencia de género. Juntos, estamos trabajando para construir un futuro en el que la igualdad de género sea una realidad y donde todas las personas puedan vivir libres de miedo y violencia.
            </p>
          </div>
        
      </Row>
      <Row>
        <Col style={{ fontFamily: 'Arial Narrow'  }}>
        <p>Desarrollado por:</p>
        </Col>
        <Col>
        <Row style={{ fontSize: '0.9rem', fontFamily: 'Arial Narrow'  }}>Carvajal Alcon Ruth Abigail</Row>
        <Row style={{ fontSize: '0.9rem', fontFamily: 'Arial Narrow'  }}>Limachi Casi Wilma</Row>
        <Row style={{ fontSize: '0.9rem', fontFamily: 'Arial Narrow'  }}>Luna Choque Alex Wilmer</Row>
        <Row style={{ fontSize: '0.9rem', fontFamily: 'Arial Narrow'  }}>Marban Callisaya Chistian</Row>
        <Row style={{ fontSize: '0.9rem', fontFamily: 'Arial Narrow'  }}>Ovando Colque Marcelo</Row>
        <Row style={{ fontSize: '0.9rem', fontFamily: 'Arial Narrow'  }}>Ticona Huacani Luis Fernando</Row>
        </Col>
        <Col>
        <Row style={{ fontSize: '0.9rem', color: '#007BFF', fontFamily: 'Arial Narrow'  }}>rcarvajala@fcpn.edu.bo</Row>
        <Row style={{ fontSize: '0.9rem', color: '#007BFF', fontFamily: 'Arial Narrow'  }}>wilmiz2423@gmail.com</Row>
        <Row style={{ fontSize: '0.9rem', color: '#007BFF', fontFamily: 'Arial Narrow'  }}>awluna1@umsa.bo</Row>
        <Row style={{ fontSize: '0.9rem', color: '#007BFF', fontFamily: 'Arial Narrow' }}>cmarban@umsa.bo</Row>
        <Row style={{ fontSize: '0.9rem', color: '#007BFF', fontFamily: 'Arial Narrow'  }}>ovandocolquemarcelo@gmail.com</Row>
        <Row style={{ fontSize: '0.9rem', color: '#007BFF', fontFamily: 'Arial Narrow' }}>lticonah@fcpn.edu.bo</Row>
        </Col>
      </Row>
      <Row>
      <p className="text-center">&copy; Copyright 2023</p>
      </Row>
    </Container>
  );
};

export default Inicio;
