import { Col, Container, Figure, Row } from "react-bootstrap";
import HM1 from "../../assets/images/HM1.jpg";
import M1 from "../../assets/images/M1.jpg";
import "atropos/css";
import Atropos from "atropos/react";

const Main = () => {
	return (
		<Container>
			<Row
				className="justify-content-md-center text-center"
				data-bs-theme="dark"
			>
				<Atropos
					activeOffset={40}
					shadowScale={1.05}
					data-bs-theme="dark"
				>
					<div className="body-main" data-bs-theme="dark">
						<div className="position-absolute top-0 start-50">
							<h1
								className="text-center text-white fs-bolder text-uppercase"
								data-atropos-offset="-4"
							>
								Violencia contra el genero
							</h1>
						</div>
						<div
							className="container-vision-mision "
							data-atropos-offset="-5"
						>
							<p className="fw-bold text-white fs-2">Visión:</p>
							<p className="fst-italic text-black">
								"Nuestra visión es un mundo donde todas las
								personas, independientemente de su género, vivan
								libres de violencia y discriminación. Deseamos
								construir una sociedad en la que la igualdad y
								el respeto mutuo sean los cimientos sobre los
								que se edifica cada relación y comunidad."
							</p>
						</div>
						<div
							className="container-vision-mision mision"
							data-atropos-offset="-5"
						>
							<p className="fw-bold text-white fs-2">Misión:</p>
							<p className="fst-italic text-black">
								"Nuestra misión es concienciar, educar y
								empoderar a individuos y comunidades en la lucha
								contra la violencia de género. A través de la
								información, la sensibilización y el apoyo, nos
								esforzamos por fomentar un cambio cultural que
								promueva la igualdad de género, el respeto y la
								justicia. Trabajamos incansablemente para
								ofrecer recursos, herramientas y apoyo emocional
								a las víctimas de la violencia de género, así
								como para promover la prevención y la
								erradicación de esta problemática en todas sus
								formas."
							</p>
						</div>
					</div>
				</Atropos>
			</Row>
			<Row className="justify-content-md-center text-center">
				<Col>
					<p className="text-start p-2">
						El respeto y la igualdad son los cimientos de relaciones
						saludables. Juntos, podemos construir un mundo libre de
						violencia de género.
					</p>
					<Atropos
						className="my-atropos"
						style={{ width: "220px", height: "220px" }}
					>
						<img
							alt="imagen"
							src={M1}
							data-atropos-offset="5"
							style={{ width: "220px", height: "220px" }}
						/>
					</Atropos>
				</Col>
				<Col>
					<Atropos
						className="my-atropos"
						style={{ width: "171px", height: "171px" }}
					>
						<Figure>
							<Figure.Image
								width={171}
								height={180}
								alt="171x180"
								src={HM1}
							/>
						</Figure>
					</Atropos>

					<p className="text-start p-2">
						Tu voz importa. Alzarla contra la violencia de género
						crea un eco de cambio en nuestra sociedad.
					</p>
				</Col>
			</Row>
		</Container>
	);
};

export default Main;
