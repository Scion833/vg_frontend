import { Card, Col, Row, Stack } from "react-bootstrap";

/**
 * img_articulo: string
 * lugar: string
 * horarios: string
 * fecha_inicio: string
 * fecha_final: string
 * descripcion: string
 *
 * onClick: function --para registrar al evento
 *
 * @param {*} props
 * @returns
 */
const ModeloEventos = (props) => {
	return (
		<>
			<Card className="m-4" border="primary" style={{ width: "18rem" }}>
				<Card.Header className="text-center">
					{props.nombre}
				</Card.Header>
				<Card.Body>
					{/* <Card.Title>{props.nombre}</Card.Title> */}
					<Card.Text style={{height:"100px",overflow:"auto"}}>{props.descripcion}</Card.Text>
					<p className="fw-light">
						<Row>
							<Col>
								<p className="text-center text-decoration-underline">
									Inicio
								</p>{" "}
								{props.fecha_inicio}
							</Col>
							<Col>
								<p className="text-center text-decoration-underline">
									Finaliza
								</p>{" "}
								{props.fecha_final}
							</Col>
						</Row>
					</p>
					<p className="text-secondary">
						{"Horarios: " + props.horarios}
					</p>
					<Stack>{props.children}</Stack>
				</Card.Body>
				<Card.Footer className="text-muted text-center">
					{"Lugar: " + props.lugar}
				</Card.Footer>
			</Card>
		</>
	);
};

export default ModeloEventos;
