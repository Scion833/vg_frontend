import {
	Button,
	Container,
	OverlayTrigger,
	Table,
	Tooltip,
} from "react-bootstrap";
import { DIRURL, urls_apis } from "../../../models/coneccion/comunicacion";
import { useEffect, useState } from "react";
import ModalInput from "../../../components/ModalInput/ModalInput";
import TableUsuarios from "./TableUsuarios";
import BtnOcultarEvento from "./BtnOcultarEvento";

const AdministrarEventos = (props) => {
	const [eventos, setEventos] = useState([]);
	const getEventos = async () => {
		await fetch(DIRURL.concat(urls_apis.GetAllEvento), {
			method: "GET",
			headers: {
				Accept: "*/*",
			},
		})
			.then((res) => {
				if (res.status === 200) {
					return res.json();
				}
			})
			.then((data) => {
				setEventos(data.data);
			})
			.catch((err) => {
				console.log(err);
			});
	};

	useEffect(() => {
		getEventos();
	}, []);

	return (
		<Container>
			<h2 className="text-center">Administrar Eventos</h2>
			<Table striped bordered hover responsive>
				<thead>
					<tr>
						<th>ID</th>
						<th>Nombre</th>
						<th>Descripcion</th>
						<th>Lugar</th>
						<th>Horarios</th>
						<th>Fecha Inicio</th>
						<th>Fecha Final</th>
						<th>Ver Participantes</th>
						<th>Estado</th>
					</tr>
				</thead>
				<tbody>
					{eventos.map((obj, index) => (
						<tr key={obj.id_evento}>
							<td>{obj.id_evento}</td>
							<td>{obj.nombre}</td>
							<td>
								<OverlayTrigger
									overlay={
										<Tooltip>{obj.descripcion}</Tooltip>
									}
								>
									<p
										className="text-truncate"
										style={{ maxWidth: "150px" }}
									>
										{obj.descripcion}
									</p>
								</OverlayTrigger>
							</td>
							<td>{obj.lugar}</td>
							<td>{obj.horarios}</td>
							<td>{obj.fecha_inicio}</td>
							<td>{obj.fecha_final}</td>
							<td>
								<ModalInput
									title="Participantes"
									icononclick={(f) => {
										return (
											<Button
												variant="primary"
												onClick={() => f()}
											>
												Ver
											</Button>
										);
									}}
									isactiveacceptbtn={false}
									textcancel={"Cerrar"}
								>
									<TableUsuarios id_evento={obj.id_evento} />
								</ModalInput>
							</td>
							<td>
								<BtnOcultarEvento id_evento={obj.id_evento} estado={obj.estado} />
							</td>
						</tr>
					))}
				</tbody>
			</Table>
		</Container>
	);
};

export default AdministrarEventos;
