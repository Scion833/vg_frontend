import { useState } from "react";
import { Button } from "react-bootstrap";
import { BiShow } from "react-icons/bi";
import { GrFormViewHide } from "react-icons/gr";
import { DIRURL, urls_apis } from "../../../models/coneccion/comunicacion";

const BtnOcultarEvento = (props) => {
	const [show, setShow] = useState(props.estado === "1");

	const eventoEstado = async (path) => {
		await fetch(path, {
			method: "PUT",
			headers: {
				Accept: "*/*",
				"x-token": localStorage.getItem("x-token"),
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				id_evento: props.id_evento,
			}),
		})
			.then((res) => {
				if (res.status === 200) {
					setShow(!show);
				}
			})
			.catch((err) => console.log(err));
	};

	return (
		<Button
			variant={show ? "success" : "danger"}
			onClick={() =>
				eventoEstado(
					show
						? DIRURL.concat(urls_apis.DeshabilitarEvento)
						: DIRURL.concat(urls_apis.HabilitarEvento)
				)
			}
		>
			{show ? (
				<BiShow size="30px" />
			) : (
				<GrFormViewHide size="30px" color="white" />
			)}
		</Button>
	);
};

export default BtnOcultarEvento;
