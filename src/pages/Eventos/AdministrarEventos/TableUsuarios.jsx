import { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import { DIRURL, urls_apis } from "../../../models/coneccion/comunicacion";

const TableUsuarios = (props) => {
	const [usuario, setUsuario] = useState([]);

	const getUsuarioByEvento = async (id_evento) => {
		await fetch(DIRURL.concat(urls_apis.GetUsuarioByEvento), {
			method: "POST",
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				id_evento,
			}),
		})
			.then((res) => {
				if (res.status === 200) {
					return res.json();
				}
			})
			.then((data) => {
				setUsuario(data.data);
			})
			.catch((err) => console.log(err));
	};

	useEffect(() => {
		getUsuarioByEvento(props.id_evento);
		// eslint-disable-next-line
	}, []);

	return (
		<Table striped bordered hover responsive>
			<thead>
				<tr>
					<th>#</th>
					<th>Ci</th>
					<th>Ex</th>
					<th>Nombre</th>
					<th>Ap. Paterno</th>
					<th>Ap. Materno</th>
					<th>Celular</th>
				</tr>
			</thead>
			<tbody>
				{usuario.map((obj, index) => (
					<tr key={index}>
						<td>{index + 1}</td>
						<td>{obj.ci}</td>
						<td>{obj.extencion}</td>
						<td>{obj.nombre}</td>
						<td>{obj.apellido_paterno}</td>
						<td>{obj.apellido_materno}</td>
						<td>{obj.celular}</td>
					</tr>
				))}
			</tbody>
		</Table>
	);
};

export default TableUsuarios;
