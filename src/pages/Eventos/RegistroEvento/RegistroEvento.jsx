import { useRef, useState } from "react";

import {
	BsFillPencilFill,
	BsFillHouseCheckFill,
	BsClock,
	BsCalendar2Plus,
	BsCalendar2Minus,
	BsPencilSquare,
} from "react-icons/bs";
import { DIRURL, urls_apis } from "../../../models/coneccion/comunicacion";
import { Button, Spinner } from "react-bootstrap";

const RegistroEvento = ({ ...props }) => {
	const [isLoading, setIsLoading] = useState(false);

	const refNombre = useRef(null);
	const refLugar = useRef(null);
	const refHorario = useRef(null);
	const refFechaInicio = useRef(null);
	const refFechaFin = useRef(null);
	const refDescripcion = useRef(null);

	const AddEvento = async () => {
		setIsLoading(true);

		let headersList = {
			Accept: "*/*",
			"x-token": localStorage.getItem("x-token"),
			"Content-Type": "application/json",
		};

		let bodyContent = JSON.stringify({
			fecha_inicio: refFechaInicio.current.value,
			nombre: refNombre.current.value,
			lugar: refLugar.current.value,
			fecha_final: refFechaFin.current.value,
			descripcion: refDescripcion.current.value,
			horarios: refHorario.current.value,
		});

		await fetch(DIRURL.concat(urls_apis.AddEvento), {
			method: "POST",
			body: bodyContent,
			headers: headersList,
		})
			.then((res) => {
				if (res.status === 200) {
					cleanInput();
					return res.json();
				}
			})
			.then((data) => {
				console.log(data);
			})
			.catch((err) => {
				console.log(err);
			})
			.finally(() => {
				setIsLoading(false);
			});

		//    let data = await response.text();
		//    console.log(data);
		//console.log(isLoading)
	};
	const cleanInput = () => {
		refNombre.current.value = "";
		refLugar.current.value = "";
		refHorario.current.value = "";
		refFechaInicio.current.value = "";
		refFechaFin.current.value = "";
		refDescripcion.current.value = "";
	};
	return (
		<div className="container">
			<div className="row my-5 justify-content-center">
				<div className="col-12 col-md-9">
					<h1 className="display-2 fw-bold text-center text-info">
						Registro de Eventos
					</h1>
				</div>
			</div>

			<div className="row justify-content-center">
				<div className="col-12 col-sm-11 col-md-9 col-lg-8 col-xl-7 col-xxl-6">
					<div className="shadow-lg border border-secondary-subtle mb-5">
						<form
							onSubmit={(e) => e.preventDefault()}
							className="was-validated my-4 mx-4"
						>
							<div className="row mb-4 gx-3">
								<div className="col-12 col-lg-4 col-xl-3 col-xxl-3">
									<label
										for="inputNombre"
										className="col-form-label col-form-label-lg"
									>
										Nombre
										<BsFillPencilFill
											className="ms-2 mb-2"
											size={23}
										/>
									</label>
								</div>
								<div className="col-12 col-sm-12 col-lg-8 col-xl-9 col-xxl-9">
									<input
										type="text"
										className="form-control form-control-lg"
										ref={refNombre}
										placeholder="Evento de libres de violencia"
										required
									/>
								</div>
							</div>

							<div className="row mb-4">
								<div className="col-12 col-lg-4 col-xl-3 col-xxl-3">
									<label
										for="inputLugar"
										className="col-form-label col-form-label-lg"
									>
										Lugar
										<BsFillHouseCheckFill
											className="ms-2 mb-2"
											size={23}
										/>
									</label>
								</div>
								<div className="col-12 col-sm-12 col-lg-8 col-xl-9 col-xxl-9">
									<input
										type="text"
										className="form-control form-control-lg"
										ref={refLugar}
										placeholder="Ciudad de La Paz"
										required
									/>
								</div>
							</div>

							<div className="row mb-4 gx-3">
								<div className="col-12 col-lg-4 col-xl-3 col-xxl-3">
									<label
										for="inputHorario"
										className="col-form-label col-form-label-lg"
									>
										Horarios
										<BsClock
											className="ms-2 mb-2"
											size={23}
										/>
									</label>
								</div>
								<div className="col-12 col-sm-12 col-lg-8 col-xl-9 col-xxl-9">
									<input
										type="time"
										className="form-control form-control-lg"
										ref={refHorario}
										placeholder="14:00 a 16:00"
										required
									/>
								</div>
							</div>

							<div className="row mb-4 gx-0">
								<div className="col-12 col-lg-5 col-xl-5 col-xxl-5">
									<label
										for="inputFechaIni"
										className="col-form-label col-form-label-lg"
									>
										Fecha de Inicio
										<BsCalendar2Plus
											className="ms-2 mb-2"
											size={23}
										/>
									</label>
								</div>
								<div className="col-8 col-sm-8 col-lg-7 col-xl-7 col-xxl-7">
									<input
										type="date"
										className="form-control form-control-lg"
										ref={refFechaInicio}
										aria-describedby="ayuda-fecha"
										required
									/>
									<div id="ayuda-fecha" className="form-text">
										Indique la fecha de inicio.
									</div>
								</div>
							</div>

							<div className="row mb-4 gx-0">
								<div className="col-12 col-lg-5 col-xl-5 col-xxl-5">
									<label
										for="inputFechaFin"
										className="col-form-label col-form-label-lg"
									>
										Fecha de Fin
										<BsCalendar2Minus
											className="ms-2 mb-2"
											size={23}
										/>
									</label>
								</div>
								<div className="col-8 col-sm-8 col-lg-7 col-xl-7 col-xxl-7">
									<input
										type="date"
										className="form-control form-control-lg"
										ref={refFechaFin}
										aria-describedby="ayuda-fecha"
										required
									/>
									<div id="ayuda-fecha" className="form-text">
										Indique la fecha de finalizacion.
									</div>
								</div>
							</div>

							<div className="row mb-4">
								<div className="col-12 col-sm-12">
									<div className="input-group input-group-lg">
										<label
											className="input-group-text"
											for="inputDesc"
										>
											Descripcion
											<BsPencilSquare
												className="ms-2"
												size={23}
											/>
										</label>
										<textarea
											className="form-control"
											ref={refDescripcion}
											rows="4"
											aria-label="Notas"
											placeholder="Escriba la descripcion"
											required
										></textarea>
									</div>
								</div>
							</div>

							<div className="row">
								<div className="col-12 d-flex justify-content-start">
									{/* <button className="btn btn-lg fs-2 text-white" onClick={()=>AddEvento()} style={{"background-color": "steelblue"}}>
                                    Registrar
                                </button> */}
									<Button
										className="btn btn-lg fs-2 text-white"
										style={{
											"background-color": "steelblue",
										}}
										size="lg"
										onClick={() => AddEvento()}
										disabled={isLoading}
									>
										{isLoading ? (
											<Spinner
												animation="grow"
												variant="danger"
											/>
										) : (
											<></>
										)}
										Registrar
									</Button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
};

export default RegistroEvento;
