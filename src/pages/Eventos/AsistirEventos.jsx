import { useState } from "react";
import { Button, Spinner } from "react-bootstrap";
import { DIRURL, urls_apis } from "../../models/coneccion/comunicacion";
import ModeloEventos from "./ModeloEventos";

/**
 * img_articulo: string
 * lugar: string
 * horarios: string
 * fecha_inicio: string
 * fecha_final: string
 * descripcion: string
 *
 * onClick: function --para registrar al evento
 *
 * @param {*} props
 * @returns
 */
const AsistirEventos = (props) => {
	const [isLoading, setIsLoading] = useState(false);
	const [asiste, setAsiste] = useState(!props.asiste);
	//console.log(props.id_usuario)

	const addUsuarioHasEvento = async (id_evento) => {
		setIsLoading(true);
		await fetch(DIRURL.concat(urls_apis.AddUsuarioHasEvento), {
			method: "POST",
			body: JSON.stringify({
				id_evento,
			}),
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
				"Content-Type": "application/json",
			},
		})
			.then((res) => {
				if (res.status === 200) {
					setAsiste(false);
					return res.json();
				} else {
					setIsLoading(false);
				}
			})
			.then((data) => {
				setIsLoading(false);
				console.log(data.msg);
			})
			.catch((err) => {
				setIsLoading(false);
				console.log("Error: ", err);
			});
	};

	const noAsistir = async (id_evento) => {
		setIsLoading(true);
		await fetch(DIRURL.concat(urls_apis.DeleteUsuarioHasEvento), {
			method: "DELETE",
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				id_evento,
			}),
		})
			.then((res) => {
				setIsLoading(false);
				if (res.status === 200) {
					setAsiste(true);
					return res.json();
				}
			})
			.then((data) => {
				console.log(data);
			})
			.catch((err) => {
				setIsLoading(false);
				console.log("Error: ", err);
			});
	};

	return (
		<>
			<ModeloEventos
				asiste={props.id_usuario}
				id_eventos={props.id_evento}
				nombre={props.nombre}
				lugar={props.lugar}
				horarios={props.horarios}
				fecha_inicio={props.fecha_inicio}
				fecha_final={props.fecha_final}
				descripcion={props.descripcion}
			>
				{asiste ? (
					<Button
						onClick={() => addUsuarioHasEvento(props.id_eventos)}
						variant="success"
						disabled={isLoading}
					>
						{isLoading ? (
							<Spinner animation="border" variant="light" />
						) : (
							<></>
						)}
						Asistir
					</Button>
				) : (
					<Button
						variant="danger"
						onClick={() => noAsistir(props.id_eventos)}
						disabled={isLoading}
					>
						{isLoading ? (
							<Spinner animation="border" variant="light" />
						) : (
							<></>
						)}
						Cancelar Asistencia
					</Button>
				)}
			</ModeloEventos>
		</>
	);
};

export default AsistirEventos;
