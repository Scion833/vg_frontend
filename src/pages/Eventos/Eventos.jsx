import { useEffect, useState } from "react";
import { DIRURL, urls_apis } from "../../models/coneccion/comunicacion";
import AsistirEventos from "./AsistirEventos";

const Eventos = (props) => {
	const [evento, setEvento] = useState([]);
	const getAllEventos = async () => {
		let headersList = {
			Accept: "*/*",
			"x-token": localStorage.getItem("x-token"),
		};

		await fetch(DIRURL.concat(urls_apis.GetEventsToAttend), {
			method: "GET",
			headers: headersList,
		})
			.then((res) => {
				if (res.status === 200) {
					return res.json();
				}
			})
			.then((data) => {
				//console.log(data.data)
				setEvento(data.data);
			})
			.catch((err) => {
				console.log(err);
			});
	};

	useEffect(() => {
		getAllEventos();
	}, []);

	return (
		<div
			className="text-center"
			style={{
				display: "flex",
				flexWrap: "wrap",
				justifyContent: "space-evenly",
			}}
		>
			{evento.map((ob, index) => {
				return (
					<AsistirEventos
						key={ob.id_evento}
						asiste={ob.id_usuario}
						id_eventos={ob.id_evento}
						nombre={ob.nombre}
						lugar={ob.lugar}
						horarios={ob.horarios}
						fecha_inicio={ob.fecha_inicio}
						fecha_final={ob.fecha_final}
						descripcion={ob.descripcion}
						onClick={() => {
							console.log("registrado");
						}}
					/>
				);
			})}
		</div>
	);
};

export default Eventos;
