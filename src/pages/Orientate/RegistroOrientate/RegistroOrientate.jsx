import {
	BsFillPencilFill,
	BsFillFilePersonFill,
	BsFillCalendarPlusFill,
	BsPencilSquare,
} from "react-icons/bs";
import ModeloTitulo from "../../TituloPagina/ModeloTitulo";
import { DIRURL, urls_apis } from "../../../models/coneccion/comunicacion";
import { useRef } from "react";
//import { Alert, Button, Spinner } from "react-bootstrap";

//

//

const RegistroOrientate = ({ ...props }) => {
	const refTitulo = useRef(null);
	const refAutor = useRef(null);
	const refFecha = useRef(null);
	const refUrl = useRef(null);
	// const refArchivo = useRef(null);
	const refDescripcion = useRef(null);


	// limpiar pantallas
	const cleanInputs = () => {
		refTitulo.current.value = "";
		refAutor.current.value="";
		refFecha.current.value="";
		refUrl.current.value="";
		refDescripcion.current.value="";

	};

	//


	const PostOrientate = async() => {
		let headersList = {
			"Accept": "*/*",
			"x-token":localStorage.getItem('x-token'),
			"Content-Type": "application/json"
		   }
		   
		   await fetch(DIRURL.concat(urls_apis.addOrientate), { 
			 method: "POST",
			 //poner los nombres identicos a los atributos de la tabla
			 body: JSON.stringify({
				titulo:refTitulo.current.value,
				autor:refAutor.current.value,
				fecha:refFecha.current.value,
				url_articulo:refUrl.current.value,
				descripcion:refDescripcion.current.value,
			 }),
			 headers: headersList
		   })

		  // .then((res) => res.json())
		  .then((res) => {
			if(res.status === 200){
				cleanInputs();
				return res.json()
			}


			/* if(res.status === 406){

			} */
			
		  }) 
		   .then((data) => console.log(data))
		   .catch((err) => console.log(err));
	
	
	
	
	
		};
	
	return (
		<div class="container">

			<ModeloTitulo titulo={"Registro Orientate"} />

			<div class="row justify-content-center">
				<div class="col-9 col-sm-7 col-md-6 col-lg-6 col-xl-5 col-xxl-4">
					<div class="shadow-lg border mb-5">
						
						<form onSubmit={(e) => e.preventDefault()} class="was-validated my-4 mx-4">
							<div class="row mb-4">
								<div class="col-12 col-lg-3 col-xl-3 col-xxl-3">
									<label
										for="inputTitulo"
										class="col-form-label"
									>
										Titulo
										<BsFillPencilFill
											class="ms-2 mb-2"
											size={18}
										/>
									</label>
								</div>
								<div class="col-12 col-sm-12 col-lg-9 col-xl-9 col-xxl-9">
									<input
										type="text"
										class="form-control"
										ref={refTitulo}
										placeholder="Articulo de la violencia"
										required
									/>
								</div>
							</div>

							<div class="row mb-4">
								<div class="col-12 col-lg-3 col-xl-3 col-xxl-3">
									<label
										for="inputAutor"
										class="col-form-label"
									>
										Autor
										<BsFillFilePersonFill
											class="ms-2 mb-2"
											size={18}
										/>
									</label>
								</div>
								<div class="col-12 col-sm-12 col-lg-9 col-xl-9 col-xxl-9">
									<input
										type="text"
										class="form-control"
										ref={refAutor}
										placeholder="Juanito Perez"
										required
									/>
								</div>
							</div>

							<div class="row mb-4">
								<div class="col-12 col-lg-4 col-xl-4 col-xxl-4">
									<label
										for="inputFecha"
										class="col-form-label"
									>
										Fecha
										<BsFillCalendarPlusFill
											class="ms-2 mb-2"
											size={18}
										/>
									</label>
								</div>
								<div class="col-9 col-sm-9 col-md-8 col-lg-8 col-xl-8 col-xxl-8">
									<input
										type="date"
										class="form-control"
										ref={refFecha}
										aria-describedby="ayuda-fecha"
										required
									/>
									<div id="ayuda-fecha" class="form-text">
										Indique la fecha por favor.
									</div>
								</div>
							</div>

							<div class="row mb-4">
								<div class="col-12 col-lg-4 col-xl-4 col-xxl-4">
									<label
										for="inputUrl"
										class="col-form-label"
									>
										Url orientate
									</label>
								</div>
								<div class="col-12 col-sm-12 col-lg-8 col-xl-8 col-xxl-8">
									<input
										type="text"
										class="form-control"
										ref={refUrl}
										placeholder="htttps://www.example.com"
										required
									/>
								</div>
							</div>

							
						{/*	
							<div class="row mb-4">
								<div class="col-12 col-sm-12">
									<label
										for="formFile"
										class="form-label fs-5"
									>
										Suba el archivo
										<BsFillFileEarmarkPostFill
											class="ms-2 mb-2"
											size={23}
										/>
									</label>
									<input
										type="file"
										class="form-control form-control-lg"
										ref={refArchivo}
										aria-describedby="inputGroupFileAddon04"
										style={{ color: "steelblue" }}
										required
									/>
									<div class="form-text text-start">
										Ingresa el archivo por favor.
									</div>
								</div>
							</div> 
						*/}

							<div class="row mb-4">
								<div class="col-12 col-sm-12">
									<div class="input-group">
										<label
											class="input-group-text"
											for="inputDesc"
										>
											Descripcion
											<BsPencilSquare
												class="ms-2"
												size={18}
											/>
										</label>
										<textarea
											class="form-control"
											ref={refDescripcion}
											rows="4"
											aria-label="Notas"
											required
										></textarea>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-12 d-flex justify-content-start">
									<button
										class="btn fs-2 text-white"
										style={{
											backgroundColor: "steelblue",
										}}
										onClick={() => {
											PostOrientate();
										}}
									> 
										Registrar
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
};

export default RegistroOrientate;
