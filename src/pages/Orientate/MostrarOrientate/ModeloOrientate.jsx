import { BsLink45Deg } from "react-icons/bs";

const ModeloOrientate = (props) => {
  return (
    <div className="" style={{ fontFamily: "Arial Narrow" ,width:"370px"}}>
      <div className="col">
        <div className="shadow-lg rounded-5">
          <div className="card rounded-5 border-3 h-100" style={{ borderColor: "#EBBEB6" }}>
            <div className="card-body">
              <div
                className="card-title rounded-top-4 fs-4 fw-semibold"
                style={{
                  color: "#EC715E",
                  lineHeight: "1.1em",
                }}
              >
                {props.titulo}
              </div>

              <div className="card-subtitle mt-3 mb-2 fs-6">
                <b className="" style={{ color: "#DCA7AE" }}>
                  Autor:{" "}
                </b>{" "}
                {props.autor}
              </div>

              <div className="card-subtitle mb-2 fs-6">
                <b className="" style={{ color: "#DCA7AE" }}>
                  Publicado:{" "}
                </b>
                {props.fecha}
              </div>

              <div className="card-text">
                <div
                  className="overflow-auto text-start mb-2"
                  style={{ maxHeight: "150px" }}
                >
                  <p className="me-1 mb-0">{props.descripcion}</p>
                </div>
              </div>

              <div className="text-end me-2">
                <a
                  className="card-link link-offset-2 link-offset-2-hover link-underline link-underline-opacity-0 link-underline-opacity-0-hover"
                  href={props.url}
                  style={{ color: "#8384D1" }}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Leer orientate completo.
                  <BsLink45Deg class="ms-1" size={16} />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModeloOrientate;
