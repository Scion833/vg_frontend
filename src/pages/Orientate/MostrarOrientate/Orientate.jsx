import ModeloTitulo from "../../TituloPagina/ModeloTitulo";
import { useEffect, useState } from "react";
import ModeloOrientate from "./ModeloOrientate";
import { DIRURL, urls_apis } from "../../../models/coneccion/comunicacion";

const Orientate = (props) => {

	const [arrayOrientate, setArrayOrientate] = useState([]);

	const getOrientate = async () => {
		let headersList = {
			Accept: "*/*",
			"User-Agent": "Thunder Client (https://www.thunderclient.com)",
		};

		await fetch(DIRURL.concat(urls_apis.getAllOrientate), {
			method: "GET",
			headers: headersList,
		})
			.then((res) => res.json())
			.then((data) => {
				setArrayOrientate(data.data);
			})
			.catch((err) => console.log(err));
	};

	useEffect(() => {
		console.log("hola")
		getOrientate();
	}, []);

	return (
		<div className="container">
			<ModeloTitulo titulo={"Orientate"} />

      <div className="row row-cols-1 row-cols-md-3 g-5 mb-5">
        {arrayOrientate.map((o, index) => {
          return (
            <ModeloOrientate
              key={o.id_orientate}
              titulo={o.titulo}
              autor={o.autor}
              descripcion={o.descripcion}
              url={o.url_articulo}
              fecha={o.fecha}
            />
          );
        })}
      </div>
		</div>
	);
};

export default Orientate;
