//import {Card } from "react-bootstrap";
import { Button, Stack } from "react-bootstrap";
import CardOrientate from "./CardOrientate";
import { DIRURL, urls_apis } from "../../../models/coneccion/comunicacion";
import { useEffect, useState } from "react";
import { Spinner } from 'react-bootstrap';
import { Alert } from 'react-bootstrap';

// ... tu código JSX aquí ...


const EditarOrientate = (props) => {
  const [arrayOrientate,setArrayOrientate] = useState([]);
  const [isLoading,setIsLoading]=useState(false);
  const [messageStatus, setMessageStatus] = useState({
		title: "",
		message: "",
		isShow: false,
		variant: "",
	});
  
  const getAllOrientate=async()=>{
    setIsLoading(true);
    setMessageStatus({
      title: "",
      message: "",
      isShow: false,
      variant: "",
    });

    await fetch(DIRURL.concat(urls_apis.getAllOrientate),{
      method:"GET",
      headers:{
        "Content-Type": "application/json",
        Accept: "*/*",

      }
    }).then(res=>{
      switch (res.status) {
        case 200:
          setMessageStatus({
            title: "",
            message: "",
            isShow: false,
            variant: "",
          });
          return res.json()
        case 409:
          setMessageStatus({
            title: "Error 409",
            message: "Error al obtener datos!",
            isShow: true,
            variant: "danger",
          })
          
          break;
        
          default:
            break;

      }
    }).then(data=>{
        setArrayOrientate(data.data);
    })
    .catch(err=>{
        console.log(err);
        setMessageStatus({
          title: "Error",
          message: "Error  al intentar obtener los datos!",
          isShow: true,
          variant: "danger",
        })
    }).finally(()=>{
        setIsLoading(false);
        console.log("Finalizó");
    });

  }

  /*
  let arrayOrientate = [
    {
      titulo: "Ser mujer como factor de riesgo en Bolivia",
      descripcion: "Un maremoto constante de casos de violencia de género embiste al país latinoamericano. La violencia sexual suma cada día 28 casos; más de uno a la hora. Frente a esta realidad, las mujeres levantan diques de contención",
      autor: "ANTONIO BALAS LECHÓN",
      imagen: "https://imagenes.elpais.com/resizer/03Wc1R3O0zlg2Lo8-NGuzVbOgIk=/980x0/cloudfront-eu-central-1.images.arcpublishing.com/prisa/UDF6IY6BOVAXXPFEEGNROPA4H4.jpg",
      url_articulo: "https://elpais.com/planeta-futuro/red-de-expertos/2022-06-30/ser-mujer-como-factor-de-riesgo-en-bolivia.html",
      fecha: "29/06/22",
      id_orientate: 1,
    },
    {
      titulo: "Una mirada a la situación de la violencia contra la mujer en Bolivia",
      descripcion: "Este trabajo tiene como objetivo exponer la actual situación de la violencia contra la mujer en la pareja, abordaje realizado de manera crítica basada en información debidamente documentada. Los informes internacionales, a nivel latinoamericano, indican que Bolivia ocupa el primer lugar en violencia física contra la mujer y el segundo lugar en violencia sexual, en las edades de 15 a 49 años. Se hace referencia a las acciones que el Estado boliviano realiza, a través de promulgación de leyes que tienen como meta garantizar una vida libre de violencia a la mujer boliviana, sin embargo, datos actuales muestran que este fenómeno se ha incrementado y recrudecido significativamente.",
      autor: "Silvia Requena Gonzáles",
      imagen: "http://....",
      url_articulo: "http://www.scielo.org.bo/scielo.php?script=sci_arttext&pid=S2223-30322017000100008",
      fecha: "15/09/23",
      id_orientate: 2,
    },
    {
      titulo: "LEY PARA GARANTIZAR A LAS MUJERES UNA VIDA LIBRE DE VIOLENCIA Nº 348",
      descripcion: "holi",
      autor: "Defensoría del Pueblo",
      imagen: "https:///.....",
      url_articulo: "https://www.defensoria.gob.bo/uploads/files/cartilla-ley-348-en-43-preguntas-y-respuestas.pdf.",
      fecha: "14/09/23",
      id_orientate: 3,
    },
  ];
  */

  useEffect(()=>{
    getAllOrientate();
  },[]);

  return (
    <>
     <h1 style={{textAlign: "center"}}>LISTADO DE ORIENTATE</h1>
     <div className="text-center">
      {isLoading?(
          <Spinner animation="grow" variant="danger" /> 
        ):(
           <></>
        )}
        {messageStatus.isShow ? (
          <Alert 
            variant="danger" >
        <Alert.Heading>{messageStatus.title}</Alert.Heading>
        <p>
          {messageStatus.message}
        </p>
        <hr />
      <p className="mb-0">
      <Button variant="outline-danger"
      onClick={getAllOrientate}>Actualizar</Button>
      </p>
      </Alert>):(<></>)}
     </div>
         
     {arrayOrientate.map((obj,index)=> {
      return(
        <Stack className="col-md-9 mx-auto"> 
          <CardOrientate 
            titulo={obj.titulo} 
            descripcion={obj.descripcion}
            autor={obj.autor}
            url_articulo={obj.url_articulo}
            fecha={obj.fecha} 
            id_orientate={obj.id_orientate}
            key={obj.id_orientate}
            />
        </Stack>
      );
      
     })}
    
    
    </>
  );
};
export default EditarOrientate;
