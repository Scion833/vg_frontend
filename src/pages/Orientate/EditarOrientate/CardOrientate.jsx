import { Card, Col, Row, Button} from "react-bootstrap";
import ModalInput from "../../../components/ModalInput/ModalInput";
import InputDataOrientates from "./InputDataOrientates";
import { useState } from "react";

const CardOrientate = (props) => {
  
  const [titulo,setTitulo]=useState(props.titulo)
  const [descripcion,setDescripcion]=useState(props.descripcion)
  const [autor,setAutor]=useState(props.autor)
  const [url_articulo,setUrl_articulo]=useState(props.url_articulo)
  const [fecha,setFecha]=useState(props.fecha)

  const refreshDate=(orientate)=>{
    setTitulo(orientate.autor);
    setDescripcion(orientate.descripcion);
    setAutor(orientate.autor);
    setUrl_articulo(orientate.url_articulo);
    setFecha(orientate.fecha);
  }

  return (
    <>
      <Card className="mb-3">
        <Card.Body>
          <Row>
            <Col>
              <Card.Title>{props.titulo}</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">
                Fecha de Publicacion :{props.fecha}
              </Card.Subtitle>
            </Col>

            <Col sm={2}>
              <ModalInput title={"Editar Orientate"} textcancel="Cerrar" isactiveacceptbtn={false} icononclick={(f) => {
                  return (
                    <Button variant="outline-info"
                    onClick={f} >
                      EDITAR
                    </Button>
                  );
              }}
              >
                <InputDataOrientates 
                titulo={titulo}
                descripcion={descripcion}
                autor={autor}
                url_articulo={url_articulo}
                fecha={fecha}
                id_orientate={props.id_orientate}
                refreshDate={refreshDate}
                />
                  
              </ModalInput>
              </Col>
              
              <Col sm={2}><Button variant="outline-danger" >ELIMINAR</Button></Col>
          </Row>
        </Card.Body>
      </Card>
    </>
  );
};
export default CardOrientate;
