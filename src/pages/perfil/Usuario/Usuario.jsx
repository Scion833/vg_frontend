import { Button, Container, Stack } from "react-bootstrap";
import { Link, useSearchParams } from "react-router-dom";

const Usuario = ({ ...props }) => {
	const [searchParams] = useSearchParams();
	const id = searchParams.get("id");
	return (
		<Container className="text-center">
			<Stack gap={2} className="col-md-5 mx-auto">
				<p className="fs-1">MENU</p>
			</Stack>

			<Stack gap={2} className="col-md-5 mx-auto">
				<Button
					size="lg"
					as={Link}
					variant="outline-primary"
					to={`/perfil/usuario/denuncia?id=${encodeURIComponent(id)}`}
				>
					DENUNCIAS
				</Button>
			</Stack>
		</Container>
	);
};

export default Usuario;
