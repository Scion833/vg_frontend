import { Button } from "react-bootstrap";
import ModalInput from "../../../../components/ModalInput/ModalInput";
import { PiUserListFill } from "react-icons/pi";
import {
	DIRURL,
	urls_apis,
} from "../../../../models/coneccion/comunicacion";
import { useState } from "react";
import TablaAgresores from "./TablaAgresores";
import Paginator from "../../../../components/Paginator/Paginator";

const VerAgresores = ({ ...props }) => {
	const [agresor, setAgresor] = useState([]);
	const [cantidadPaginas,setCantidadPaginas]=useState(0)

	const getAgresores = async (cantidad, desde) => {
		await fetch(DIRURL.concat(urls_apis.getAgresorByIdDenuncia), {
			method: "POST",
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				id_denuncia: props.id_denuncia,
				cantidad,
				desde,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data?.data) {
					setAgresor(data.data);
					setCantidadPaginas(Math.ceil(data.cantidad/5))
				}
			})
			.catch((err) => {
				console.log(err);
			});
	};

	return (
		<ModalInput
			title="Agresores"
			isactiveacceptbtn={false}
			textcancel={"cerrar"}
			icononclick={(f) => {
				return (
					<Button
						variant="primary"
						onClick={() => {
							getAgresores(5, 0);
							f();
						}}
					>
						<PiUserListFill />
					</Button>
				);
			}}
		>
			<TablaAgresores agresor={agresor} />
			<Paginator
				size={cantidadPaginas}
				key={"agresor"+cantidadPaginas}
				onClick={(pagNumber) => {
					getAgresores(5, (pagNumber - 1) * 5);
				}}
			/>
		</ModalInput>
	);
};

export default VerAgresores;
