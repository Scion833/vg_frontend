import { Alert, Button, Form, Spinner, Stack } from "react-bootstrap";
import InputRowComponent from "../../../../components/InputRowComponent/InputRowComponent";
import ModalInput from "../../../../components/ModalInput/ModalInput";
import { useRef, useState } from "react";
import { DIRURL, urls_apis } from "../../../../models/coneccion/comunicacion";
import { useSearchParams } from "react-router-dom";

const AgregarDenuncia = ({ ...props }) => {
	const [showAlert, setShowAlert] = useState({
		show: false,
		mensaje: "",
		variant: "danger",
	});
	const [loading, setLoading] = useState(false);

	const [searchParams] = useSearchParams();
	const id = searchParams.get("id");

	const refTipo = useRef(null);
	const refDescripcion = useRef(null);
	const refUbicacion = useRef(null);
	const refFecha = useRef(null);
	const refAnonimo = useRef(null);

	const getDenuncia = () => {
		let id_usuario = id ?? "0";
		let tipo = refTipo.current.value();
		let descripcion = refDescripcion.current.value();
		let ubicacion = refUbicacion.current.value();
		let fecha = refFecha.current.value();
		let anonimo = refAnonimo.current.checked?"1":"0";
		return { id_usuario, tipo, descripcion, ubicacion, fecha, anonimo };
	};

	const addDenun = async () => {
		setLoading(true);
		await fetch(DIRURL.concat(urls_apis.addDenuncia), {
			method: "POST",
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
				"Content-Type": "application/json",
			},
			body: JSON.stringify(getDenuncia()),
		})
			.then((res) => {
				if (res.status === 200) {
					setShowAlert({
						show: true,
						mensaje: "Se agrego correctamente",
						variant: "success",
					});
				} else {
					setShowAlert({
						show: true,
						mensaje: "Ocurrio un error",
						variant: "danger",
					});
				}
				setLoading(false);
			})
			.catch((err) => {
				console.error(err);
				setLoading(false);
				setShowAlert({
					show: true,
					mensaje: "Ocurrio un error al enviar los datos",
					variant: "danger",
				});
			});
	};

	return (
		<ModalInput
			title={"Agregar Denuncia"}
			icononclick={(f) => {
				return (
					<Button variant="primary" onClick={f}>
						Agregar denuncia
					</Button>
				);
			}}
			isactiveacceptbtn={false}
			textcancel={"Cerrar"}
		>
			<InputRowComponent ref={refTipo} label={"tipo:"} />
			<InputRowComponent ref={refDescripcion} label={"Descripcion:"} />
			<InputRowComponent ref={refUbicacion} label={"Ubicacion:"} />
			<InputRowComponent ref={refFecha} type={"date"} label={"fecha:"} />
			<br />

			<div className="text-center">
				<Form.Check
					inline
					label="Denunciar como Anonimo"
					name="grupo1"
					type="checkbox"
					id={"anonimo"}
					ref={refAnonimo}
				/>
			</div>
			<br />
			{showAlert.show ? (
				<Alert
					variant={showAlert.variant}
					onClose={() =>
						setShowAlert({
							show: false,
							mensaje: "",
							variant: "danger",
						})
					}
					dismissible
				>
					<p className="text-center">{showAlert.mensaje}</p>
				</Alert>
			) : (
				<></>
			)}
			<Stack gap={2} className="col-md-5 mx-auto">
				<Button
					variant="primary"
					onClick={() => {
						addDenun();
					}}
					disabled={loading}
				>
					{loading ? (
						<Spinner animation="grow" variant="light" />
					) : (
						<></>
					)}
					Agregar Denuncia
				</Button>
			</Stack>
		</ModalInput>
	);
};

export default AgregarDenuncia;
