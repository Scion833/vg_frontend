import { Button } from "react-bootstrap";
import ModalInput from "../../../../components/ModalInput/ModalInput";
import { PiUserList } from "react-icons/pi";
import TablaVictima from "./TablaVictima";
import { useState } from "react";
import {
	DIRURL,
	headers,
	urls_apis,
} from "../../../../models/coneccion/comunicacion";
import Paginator from "../../../../components/Paginator/Paginator";

const VerVictima = ({ ...props }) => {
	const [victima, setVictima] = useState([]);
	const [cantPg, setCantPg] = useState(0);

	const getVictimas = async (cantidad, desde) => {
		await fetch(DIRURL.concat(urls_apis.getVictimaByIDdenuncia), {
			method: "POST",
			headers: headers,
			body: JSON.stringify({
				id_denuncia: props.id_denuncia,
				cantidad,
				desde,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				setVictima(data?.data ?? []);
				setCantPg(Math.ceil((data?.cantidad ?? 0) / 5));
			})
			.catch((err) => {
				console.error(err);
			});
	};

	return (
		<ModalInput
			title={"Lista de Victimas"}
			textcancel={"Cerrar"}
			isactiveacceptbtn={false}
			icononclick={(f) => {
				return (
					<Button
						variant="primary"
						onClick={() => {
							getVictimas(5, 0);
							f();
						}}
					>
						<PiUserList />
					</Button>
				);
			}}
		>
			<TablaVictima victima={victima} />
			<Paginator
				size={cantPg}
				key={"victima" + cantPg}
				onClick={(pagNumber) => {
					getVictimas(5, (pagNumber - 1) * 5);
				}}
			/>
		</ModalInput>
	);
};

export default VerVictima;
