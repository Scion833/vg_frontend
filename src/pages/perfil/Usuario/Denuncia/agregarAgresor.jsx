import { Button, Spinner, Stack } from "react-bootstrap";
import ModalInput from "../../../../components/ModalInput/ModalInput";
import InputRowComponent from "../../../../components/InputRowComponent/InputRowComponent";
import { useRef, useState } from "react";
import {
	DIRURL,
	urls_apis,
} from "../../../../models/coneccion/comunicacion";

const AgregarAgresor = ({ ...props }) => {
	const [loading, setLoading] = useState(false);

	const refNombre = useRef(null);
	const refPaterno = useRef(null);
	const refMaterno = useRef(null);
	const refDescripcion = useRef(null);

	const addAgresor = async () => {
		setLoading(true);
		let nombre = refNombre.current.value();
		let apellido_paterno = refPaterno.current.value();
		let apellido_materno = refMaterno.current.value();
		let descripcion = refDescripcion.current.value();
		await fetch(DIRURL.concat(urls_apis.addAgresor), {
			method: "POST",
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				id_denuncia: props.id_denuncia,
				nombre,
				apellido_paterno,
				apellido_materno,
				descripcion,
			}),
		})
			.then((res) => {
				setLoading(false);
				if (res.status === 200) {
					console.log("ingresado");
				}
			})
			.catch((err) => {
				setLoading(false);
				console.log(err);
			});
	};

	return (
		<ModalInput
			title={"Agresores"}
			isactiveacceptbtn={false}
			textcancel={"cerrar"}
			icononclick={(f) => {
				return (
					<Button variant="success" onClick={f}>
						Agregar
					</Button>
				);
			}}
		>
			<InputRowComponent ref={refNombre} label={"Nombre:"} />
			<InputRowComponent ref={refPaterno} label={"Apellido Paterno:"} />
			<InputRowComponent ref={refMaterno} label={"Apellido Materno:"} />
			<InputRowComponent ref={refDescripcion} label={"Descripcion:"} />
			<br />
			<Stack gap={2} className="col-md-5 mx-auto">
				<Button
					variant="primary"
					onClick={() => {
						addAgresor();
					}}
					disabled={loading}
				>
					{loading ? (
						<Spinner animation="grow" variant="light" />
					) : (
						<></>
					)}
					Agregar Agresor
				</Button>
			</Stack>
		</ModalInput>
	);
};

export default AgregarAgresor;
