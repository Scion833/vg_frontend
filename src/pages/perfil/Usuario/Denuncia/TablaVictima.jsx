import { Table } from "react-bootstrap"

const TablaVictima=({...props})=>{
	return(
		<Table striped bordered hover responsive>
			<thead>
				<tr>
					<th>#</th>
					<th>Nombre</th>
					<th>Apellido Paterno</th>
					<th>Apellido Materno</th>
					<th>Descripcion</th>
					<th>fecha de nacimiento</th>
				</tr>
			</thead>
			<tbody>
				{props.victima.map((obj,index)=>(
					<tr key={"victima"+obj.id_victima}>
						<td>{obj.num}</td>
						<td>{obj.nombre}</td>
						<td>{obj.apellido_paterno}</td>
						<td>{obj.apellido_materno}</td>
						<td>{obj.descripcion}</td>
						<td>{obj.fecha_nac}</td>
					</tr>
				))}
			</tbody>
		</Table>
	)
}

export default TablaVictima