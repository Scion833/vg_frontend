
import { Button, Table } from "react-bootstrap";
import { DIRURL, urls_apis } from "../../../../models/coneccion/comunicacion";

const TablaPruebasAgregadas = (props) => {

	const DeleteVinculo=async(id_denuncia,id_prueba)=>{
		await fetch(DIRURL.concat(urls_apis.deleteDenunciaHasPrueba),{
			method:"DELETE",
			headers:{
				"x-token":localStorage.getItem('x-token'),
				Accept:"*/*",
				"Content-Type":"application/json"
			},
			body:JSON.stringify({
				id_denuncia,
				id_prueba
			})
		}).then(res=>res.json()).then(data=>{
			
			props.get_pruebas(5,0)
		}).catch(err=>console.log(err))
	}

	return (
		<Table striped bordered hover responsive>
			<thead>
				<tr>
					<th>Descripcion</th>
					<th>Desvincular</th>
				</tr>
			</thead>
			<tbody>
				{props.pruebas?.map((obj, index) => (
					<tr key={"pruebas" + obj.id_prueba}>
						<td>{obj.descripcion}</td>
						<td>
							<Button
								variant="danger"
								onClick={() => {
									//vincularPrueba(obj.id_prueba);
									DeleteVinculo(props.id_denuncia,obj.id_prueba)
								}}
							>
								Desvincular
							</Button>
						</td>
					</tr>
				))??<></>}
			</tbody>
		</Table>
	);
};



export default TablaPruebasAgregadas;
