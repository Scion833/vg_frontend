import { Table } from "react-bootstrap";

const TablaAgresores = ({ ...props }) => {
	return (
		<Table striped bordered hover responsive>
			<thead>
				<tr>
					<th>#</th>
					<th>Nombre</th>
					<th>Apellido Paterno</th>
					<th>Apellido Materno</th>
					<th>Descripcion</th>
				</tr>
			</thead>
			<tbody>
				{props.agresor.map((obj, index) => {
					return (
						<tr key={"agresor" + obj.id_agresor}>
							<td>{obj.num}</td>
							<td>{obj.nombre}</td>
							<td>{obj.apellido_paterno}</td>
							<td>{obj.apellido_materno}</td>
							<td>{obj.descripcion}</td>
						</tr>
					);
				})}
			</tbody>
		</Table>
	);
};

export default TablaAgresores;
