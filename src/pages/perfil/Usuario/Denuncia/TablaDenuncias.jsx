import { Table } from "react-bootstrap";
import AgregarAgresor from "./agregarAgresor";
import VerAgresores from "./verAgresores";
import AgregarVictima from "./AgregarVictima";
import VerVictima from "./VerVictimas";
import VincularPrueba from "./VincularPrueba";
import ListaPruebas from "./ListaPruebas";

const TablaDenuncias = ({ ...props }) => {
	return (
		<Table striped bordered hover responsive>
			<thead>
				<tr>
					<th>#</th>
					<th>tipo</th>
					<th>descripcion</th>
					<th>Ubicacion</th>
					<th>fecha</th>
					<th>Agresor</th>
					<th>Victima</th>
					<th>Testigo</th>
					<th>Vincular pruebas</th>
				</tr>
			</thead>
			<tbody>
				{props.denuncias.map((obj, index) => (
					<tr key={"denuncia" + obj.id_denuncia}>
						<td>{obj.num}</td>
						<td>{obj.tipo}</td>
						<td>{obj.descripcion}</td>
						<td>{obj.ubicacion}</td>
						<td>{obj.fecha}</td>
						<td>
							<AgregarAgresor id_denuncia={obj.id_denuncia} />
							<VerAgresores id_denuncia={obj.id_denuncia} />
						</td>
						<td>
							<AgregarVictima id_denuncia={obj.id_denuncia} />
							<VerVictima id_denuncia={obj.id_denuncia} />
						</td>
						<td>
							proximamente
						</td>
						<td>
							<VincularPrueba id_denuncia={obj.id_denuncia}/>
							<ListaPruebas id_denuncia={obj.id_denuncia}/>
						</td>
					</tr>
				))}
			</tbody>
		</Table>
	);
};

export default TablaDenuncias;
