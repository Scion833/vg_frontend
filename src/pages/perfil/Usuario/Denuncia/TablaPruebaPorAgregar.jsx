import { Button, Table } from "react-bootstrap";
import { DIRURL, urls_apis } from "../../../../models/coneccion/comunicacion";
import { BiShow } from "react-icons/bi";

const TablaPruebasPorAgregar = ({ ...props }) => {
	const vincularPrueba = async (id_prueba) => {
		await fetch(DIRURL.concat(urls_apis.addDenunciaHasPrueba), {
			method: "POST",
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				id_prueba,
				id_denuncia: props.id_denuncia,
			}),
		})
			.then((res) => {
				if (res.status === 200) {
					props.get_pruebas(5, 0);
				}
			})
			.catch((err) => {
				console.log(err);
			});
	};


	return (
		<Table striped bordered hover responsive>
			<thead>
				<tr>
					<th>Descripcion</th>
					<th>Ver</th>
					<th>Vincular</th>
				</tr>
			</thead>
			<tbody>
				{props.pruebas?.map((obj, index) => (
					<tr key={"pruebas" + obj.id_prueba}>
						<td>{obj.descripcion}</td>
						<td>
							<Button
								variant="primary"
								onClick={() => props.vararchivo(obj.url_file)}
							>
								<BiShow />
							</Button>
						</td>
						<td>
							<Button
								variant="success"
								onClick={() => {
									vincularPrueba(obj.id_prueba);
								}}
							>
								Vincular
							</Button>
						</td>
					</tr>
				)) ?? <></>}
			</tbody>
		</Table>
	);
};

export default TablaPruebasPorAgregar;
