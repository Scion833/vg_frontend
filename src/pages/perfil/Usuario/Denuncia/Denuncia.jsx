import { Container, Row, Stack } from "react-bootstrap";
import AgregarDenuncia from "./agregarDenuncia";
import TablaDenuncias from "./TablaDenuncias";
import Paginator from "../../../../components/Paginator/Paginator";
import { useEffect, useState } from "react";
import {
	DIRURL,
	urls_apis,
} from "../../../../models/coneccion/comunicacion";
import { useSearchParams } from "react-router-dom";
import AgregarPrueba from "./AgregarPrueba";

const Denuncia = ({ ...props }) => {
	const [cantidadPaginas, setCantidadPaginas] = useState(0);
	const [denuncias, setDenuncias] = useState([]);

	const [searchParams] = useSearchParams();

	const callDenuncias = async (cantidad, desde) => {
		await fetch(DIRURL.concat(urls_apis.getDenunciaByIdUsuario), {
			method: "POST",
			headers: {
				Accept: "*/*",
				"Content-Type": "application/json",
				"x-token": localStorage.getItem("x-token"),
			},
			body: JSON.stringify({
				id_usuario: searchParams.get("id") ?? "0",
				cantidad,
				desde,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				setDenuncias(data?.data ?? []);
				setCantidadPaginas(Math.ceil(data.cantidad / 5));
			})
			.catch((err) => {
				console.log(err);
			});
	};

	useEffect(() => {
		callDenuncias(5, 0);
		//eslint-disable-next-line
	}, []);

	return (
		<Container>
			<Stack gap={2} className="col-md-5 mx-auto">
				<p className="text-center fs-2">SUS DENUNCIAS</p>
			</Stack>
			<Stack gap={2} className="col-md-5 mx-auto mb-5">
				<AgregarDenuncia />
			</Stack>
			<Stack gap={2} className="col-md-5 mx-auto mb-5">
				<AgregarPrueba />
			</Stack>
			<Row>
				<TablaDenuncias denuncias={denuncias} />
				<Paginator
					size={cantidadPaginas}
					key={cantidadPaginas}
					onClick={(pagNumber) => {
						callDenuncias(5, (pagNumber - 1) * 5);
					}}
				/>
			</Row>
		</Container>
	);
};

export default Denuncia;
