import { Button } from "react-bootstrap";
import ModalInput from "../../../../components/ModalInput/ModalInput";
import { PiFilesThin } from "react-icons/pi";

const ListaPruebas = (props) => {
	return (
		<ModalInput
			title={"Agregar archivos como pruebas"}
			icononclick={(f) => {
				return (
					<Button variant="primary" onClick={f}>
						<PiFilesThin size={"25px"} />
					</Button>
				);
			}}
			isactiveacceptbtn={false}
			textcancel={"Cerrar"}
		></ModalInput>
	);
};

export default ListaPruebas;
