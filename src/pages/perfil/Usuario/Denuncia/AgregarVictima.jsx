import { Button, Spinner, Stack } from "react-bootstrap";
import ModalInput from "../../../../components/ModalInput/ModalInput";
import { AiOutlineUsergroupAdd } from "react-icons/ai";
import InputRowComponent from "../../../../components/InputRowComponent/InputRowComponent";
import { useRef, useState } from "react";
import {
	DIRURL,
	urls_apis,
} from "../../../../models/coneccion/comunicacion";

const AgregarVictima = ({ ...props }) => {
	const [loading, setLoading] = useState(false);
	const refNombre = useRef(null);
	const refPaterno = useRef(null);
	const refMaterno = useRef(null);
	const refDescripcion = useRef(null);
	const refFecha = useRef(null);

	const addVictima = async () => {
		let nombre = refNombre.current.value();
		let paterno = refPaterno.current.value();
		let materno = refMaterno.current.value();
		let descripcion = refDescripcion.current.value();
		let fecha_nac = refFecha.current.value();

		setLoading(true);

		await fetch(DIRURL.concat(urls_apis.addVictima), {
			method: "POST",
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				id_denuncia: props.id_denuncia,
				nombre,
				apellido_paterno: paterno,
				apellido_materno: materno,
				descripcion,
				fecha_nac,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				//console.log(data.mensaje);
				setLoading(false);
			})
			.catch((err) => {
				console.error(err);
				setLoading(false);
			});
	};

	return (
		<ModalInput
			title={"Agregar Victima"}
			textcancel="Cerrar"
			isactiveacceptbtn={false}
			icononclick={(f) => {
				return (
					<Button variant="success" onClick={f}>
						<AiOutlineUsergroupAdd />
					</Button>
				);
			}}
		>
			<InputRowComponent ref={refNombre} label={"Nombre:"} />
			<InputRowComponent ref={refPaterno} label={"Apellido Paterno:"} />
			<InputRowComponent ref={refMaterno} label={"Apellido Materno:"} />
			<InputRowComponent ref={refDescripcion} label={"Descripcion"} />
			<InputRowComponent
				ref={refFecha}
				label={"Fecha de Nacimiento"}
				type={"date"}
			/>
			<br />
			<Stack gap={2} className="col-md-5 mx-auto">
				<Button
					variant="primary"
					onClick={() => {
						addVictima();
					}}
					disabled={loading}
				>
					{loading ? (
						<Spinner animation="grow" variant="light" />
					) : (
						<></>
					)}
					Agregar Victima
				</Button>
			</Stack>
		</ModalInput>
	);
};

export default AgregarVictima;
