import { Button, Spinner, Stack } from "react-bootstrap";
import ModalInput from "../../../../components/ModalInput/ModalInput";
import InputRowComponent from "../../../../components/InputRowComponent/InputRowComponent";
import { useRef, useState } from "react";
import { LuFileUp } from "react-icons/lu";
import { VscNewFile } from "react-icons/vsc";
import { DIRURL, urls_apis } from "../../../../models/coneccion/comunicacion";

const AgregarPrueba = (props) => {
	const refDescripcion = useRef(null);
	const refFile = useRef(null);
	const [isLoading, setIsLoading] = useState(false);

	const subirArchivo = async () => {
		let file = refFile.current.files[0];
		if (file) {
			setIsLoading(true);
			let bodyContent = new FormData();
			bodyContent.append("descripcion", refDescripcion.current.value());
			bodyContent.append("archivo", file);
			console.log(
				refFile.current.src,
				refFile.current.value,
				bodyContent
			);
			await fetch(DIRURL.concat(urls_apis.addPrueba), {
				headers: {
					"x-token": localStorage.getItem("x-token"),
					Accept: "*/*",
				},
				method: "POST",
				body: bodyContent,
			})
				.then((res) => res.json())
				.then((data) => {
					console.log(data);
					setIsLoading(false);
				})
				.catch((err) => {
					setIsLoading(false);
					console.log(err);
				});
		}
	};

	return (
		<ModalInput
			title={"Agregar archivos como pruebas"}
			icononclick={(f) => {
				return (
					<Button
						className="text-center"
						variant="secondary"
						onClick={f}
					>
						<LuFileUp size={"20px"} />
						{"    "}
						Subir Archivos
					</Button>
				);
			}}
			isactiveacceptbtn={false}
			textcancel={"Cerrar"}
		>
			<InputRowComponent
				ref={refDescripcion}
				label="Descripcion:"
				placeholder="Descripcion del archivo"
			/>
			<br />
			<input className="mb-3" ref={refFile} type="file" />
			<br />
			<br />
			<Stack>
				<Button
					variant="success"
					onClick={subirArchivo}
					disabled={isLoading}
				>
					{isLoading ? (
						<Spinner
							as="span"
							animation="grow"
							size="sm"
							role="status"
							aria-hidden="true"
						/>
					) : (
						<VscNewFile size={"20px"} />
					)}{" "}
					Almacenar Archivo
				</Button>
			</Stack>
		</ModalInput>
	);
};

export default AgregarPrueba;
