import { Button, Col, Container, Row } from "react-bootstrap";
import ModalInput from "../../../../components/ModalInput/ModalInput";
import { VscFileMedia } from "react-icons/vsc";
import TablaPruebasPorAgregar from "./TablaPruebaPorAgregar";
import TablaPruebasAgregadas from "./TablaPruebasAgregadas";
import { useState } from "react";
import ShowMedia from "../../../../components/ShowMedia/ShowMedia";
import { DIRURL, urls_apis } from "../../../../models/coneccion/comunicacion";
import Paginator from "../../../../components/Paginator/Paginator";

const VincularPrueba = ({ ...props }) => {
	const [verprueba, setVerprueba] = useState("");
	const [pruebas, setPruebas] = useState([]);
	const [pruebasAgregadas, setPruebasAgregadas] = useState([]);

	const [sizePruebas, setSizePruebas] = useState(0);

	const [sizePruebasAgregadas, setSizePruebasAgregadas] = useState(0);

	const verArchivo = (url) => {
		setVerprueba(url);
	};

	let getPruebas = async (cantidad, desde) => {
		await fetch(DIRURL.concat(urls_apis.getPruebaByIdUsuarioAndDenuncia), {
			method: "POST",
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				cantidad,
				desde,
				id_denuncia: props.id_denuncia,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				setPruebas(data.data ?? []);
				setSizePruebas(Math.ceil((data.cantidad ?? 0) / 5));
			})
			.catch((err) => {
				console.log(err);
			});
	};

	//****************************** */

	let getPruebasAgregadas = async (cantidad, desde) => {
		await fetch(
			DIRURL.concat(urls_apis.getPruebaAgregadaByIdUsuarioAndDenuncia),
			{
				method: "POST",
				headers: {
					"x-token": localStorage.getItem("x-token"),
					Accept: "*/*",
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					cantidad,
					desde,
					id_denuncia: props.id_denuncia,
				}),
			}
		)
			.then((res) => res.json())
			.then((data) => {
				setPruebasAgregadas(data.data ?? []);
				setSizePruebasAgregadas(Math.ceil((data.cantidad ?? 0) / 5));
			})
			.catch((err) => {
				console.log(err);
			});
	};

	return (
		<ModalInput
			key={"pruebas" + props.id_denuncia}
			title={"Agregar archivos como pruebas"}
			size="lg"
			centered
			icononclick={(f) => {
				return (
					<Button
						variant="success"
						onClick={() => {
							getPruebas(5, 0);
							getPruebasAgregadas(5, 0);
							f();
						}}
					>
						<VscFileMedia size={"25px"} />
					</Button>
				);
			}}
			isactiveacceptbtn={false}
			textcancel={"Cerrar"}
		>
			<Container>
				<Row>
					<ShowMedia key={verprueba} namefile={verprueba} />
				</Row>
				<Row>
					<Col>
						<Row>
							<TablaPruebasPorAgregar
								key={"porAgregar" + props.id_denuncia}
								vararchivo={verArchivo}
								id_denuncia={props.id_denuncia}
								pruebas={pruebas}
								get_pruebas={(cantidad, desde) => {
									getPruebas(5, 0);
									getPruebasAgregadas(cantidad, desde);
								}}
							/>
						</Row>
						<Row>
							<Paginator
								key={
									"p" + props.id_denuncia + "-" + sizePruebas
								}
								size={sizePruebas}
								onClick={(num) => {
									getPruebas(5, (num - 1) * 5);
								}}
							/>
						</Row>
					</Col>
					<Col>
						<TablaPruebasAgregadas
							id_denuncia={props.id_denuncia}
							key={"agregadas" + props.id_denuncia}
							pruebas={pruebasAgregadas}
							get_pruebas={(cantidad, desde) => {
								getPruebasAgregadas(5, 0);
								getPruebas(cantidad, desde);
							}}
						/>
						<Row>
							<Paginator
								key={
									"pa" +
									props.id_denuncia +
									"-" +
									sizePruebasAgregadas
								}
								size={sizePruebasAgregadas}
								onClick={(num) => {
									getPruebasAgregadas(5, (num - 1) * 5);
								}}
							/>
						</Row>
					</Col>
				</Row>
			</Container>
		</ModalInput>
	);
};

export default VincularPrueba;
