import { useRef, useState } from "react";
import { Alert, Form, Nav } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import { DIRURL, headers, urls_apis } from "../../models/coneccion/comunicacion";

const Login = ({ ...props }) => {
	const [showAlert, setShowAlert] = useState(false);
	const [alertMsg, setAlertMsg] = useState("");

	const refUsername = useRef(null);
	const refPassword = useRef(null);

	const navigate = useNavigate();

	const btnEntrar = () => {
		let correo = refUsername.current.value;
		let password = refPassword.current.value;
		callLogin(correo, password);
	};

	const callLogin = async (correo, password) => {
		await fetch(DIRURL.concat(urls_apis.loginUsuario), {
			method: "POST",
			headers,
			body: JSON.stringify({
				correo,
				password,
			}),
		})
			.then((res) => {
				if (res.status === 200) {
					return res.json();
				} else {
					setShowAlert(true);
					switch (res.status) {
						case 401:
							console.log("Error al ingresar sus credenciales");
							setAlertMsg("Error al ingresar sus credenciales");
							break;
						case 404:
							console.log("Usuario no encontrado");
							setAlertMsg("Usuario no encontrado");
							break;

						default:
							setAlertMsg("Ocurrio algun error");
							break;
					}
				}
			})
			.then((data) => {
				if (data) {
					localStorage.setItem("x-token", data["x-token"]);
					props.updateToken(data["x-token"]);
					navigate("/");
				}
			})
			.catch((err) => console.log(err));
	};

	//let text=usernameRef.current.value

	return (
		<div className="d-flex justify-content-center align-items-center">
			<div className="container">
				<div className="row d-flex justify-content-center">
					<div className="col-12 col-md-8 col-lg-6">
						<div className="border border-3 border-primary"></div>
						<div className="card  shadow-lg">
							<div className="card-body p-5">
								<div className="mb-3 mt-md-4">
									<h2 className="fw-bold mb-2 text-uppercase ">
										Iniciar Sesion
									</h2>

									<p className=" mb-5">
										Por favor ingrese su correo y
										contraseña!
									</p>

									<div className="mb-3">
										<label
											htmlFor="email"
											className="form-label "
										>
											Correo electronico
										</label>

										<Form.Control
											ref={refUsername}
											type="email"
											id="email"
											placeholder="Nombre@Ejemplo.com"
										/>

										<div
											id="ayuda-correo"
											className="form-text"
										>
											Ingrese su correo.
										</div>
									</div>

									<div className="mb-3">
										<label
											htmlFor="password"
											className="form-label "
										>
											Contraseña
										</label>

										<Form.Control
											ref={refPassword}
											type="password"
											className="form-control"
											id="password"
											placeholder="***************"
										/>

										<div
											id="ayuda-correo"
											className="form-text"
										>
											Ingrese su contraseña.
										</div>
									</div>

									<Alert
										variant="danger"
										onClose={() => {
											setShowAlert(false);
										}}
										dismissible
										show={showAlert}
									>
										<p className="text-center">
											{alertMsg}
										</p>
									</Alert>

									<div className="d-grid">
										<button
											className="btn btn-primary"
											onClick={btnEntrar}
										>
											{" "}
											Login
										</button>
									</div>
								</div>

								<div>
									<p className="mb-0  text-center">
										No tiene cuenta?
										<Nav.Link
											as={Link}
											to="/Registrar"
											className="text-decoration-underline text-primary-emphasis"
										>
											Registrarse
										</Nav.Link>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Login;
