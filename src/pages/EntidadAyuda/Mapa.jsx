import React, { useEffect, useState } from "react";
import "leaflet/dist/leaflet.css";
import {
	MapContainer,
	TileLayer,
	Marker,
	Popup,
	Circle,
	LayerGroup,
	useMapEvents,
} from "react-leaflet";
import L from "leaflet";
import mod from "./mod.png";
import ayuda from "./ayuda.png";
import centroMedico from "./centro_medico.png";
import silueta from "./silueta.png";
import { DIRURL, urls_apis } from "../../models/coneccion/comunicacion";
import { useSearchParams } from "react-router-dom";

// Elimina la configuración predeterminada del icono de Leaflet
delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
	iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
	iconUrl: require("leaflet/dist/images/marker-icon.png"),
	shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
});

const Mapa = (props) => {
	const [userLocation, setUserLocation] = useState(null);
	const [arrayEntidades, setArrayEntidades] = useState([]);
	const [searchParams] = useSearchParams();
	let latitud = searchParams.get("latitud");
	let longitud = searchParams.get("longitud");

	useEffect(() => {
		// Usar la geolocalización del navegador para obtener la ubicación del usuario
		navigator.geolocation.getCurrentPosition((position) => {
			setUserLocation([
				position.coords.latitude,
				position.coords.longitude,
			]);
		});
	}, []);

	const position = userLocation || [-16.499728, -68.14154];
	//aqui van las posiciones de los vectores latitud y longitud este es solo de ejemplo

	var LeafIcon = L.Icon.extend({
		options: {
			iconUrl: mod,
			iconSize: [50, 50],
			iconAnchor: [25, 50],
		},
	});

	var LeafIcon2 = L.Icon.extend({
		options: {
			iconUrl: ayuda,
			iconSize: [50, 50],
			iconAnchor: [25, 50],
		},
	});

	var LeafIcon3 = L.Icon.extend({
		options: {
			iconUrl: centroMedico,
			iconSize: [50, 50],
			iconAnchor: [22, 50],
		},
	});

	var LeafIcon4 = L.Icon.extend({
		options: {
			iconUrl: silueta,
			iconSize: [40, 40],
			iconAnchor: [20, 40],
		},
	});

	const moduloicon = new LeafIcon({ iconUrl: mod }); // Cambio de icomod a siluetaicon
	const ayudaicon = new LeafIcon2({ iconUrl: ayuda });
	const centroicon = new LeafIcon3({ iconUrl: centroMedico });
	const siluetaicon = new LeafIcon4({ iconUrl: silueta });

	const getIcon = (tipo) => {
		switch (tipo) {
			case "Modulo Policial":
				return moduloicon;
			case "Centro de Ayuda":
				return ayudaicon;
			case "Centro Medico":
				return centroicon;
			case "Otros":
				return siluetaicon;

			default:
				return siluetaicon;
		}
	};

	const getEntidadAyuda = async () => {
		await fetch(DIRURL.concat(urls_apis.GetAllEntidad_Ayuda), {
			method: "GET",
			headers: {
				Accept: "*/*",
			},
		})
			.then((res) => {
				if (res.status === 200) {
					return res.json();
				}
			})
			.then((data) => {
				console.log(data);
				setArrayEntidades(data.data);
			})
			.catch((err) => {
				console.log(err);
			});
	};

	useEffect(() => {
		getEntidadAyuda();
	}, []);

	return (
		<div style={{ display: "grid", height: "400px", width: "100%" }}>
			<MapContainer center={position} zoom={13}>
				<TileLayer
					attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
					url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
				/>
				{userLocation && (
					<>
						<Marker position={userLocation} icon={siluetaicon}>
							<Popup>Tu ubicación actual</Popup>
						</Marker>
					</>
				)}
				<LayerGroup>
					<Circle
						center={position}
						pathOptions={{ fillColor: "blue" }}
						radius={500}
					>
						<Popup>Radio Azul: 500 metros</Popup>
					</Circle>
				</LayerGroup>

				{/* vectores de las ubicaciones */}
				{arrayEntidades.map((obj, index) => {
					let pos = [obj.latitud, obj.longitud];
					return (
						<Marker
							key={obj.id_entidad}
							position={pos}
							icon={getIcon(obj.tipo)}
						>
							<Popup>
								<b>{obj.nombre}</b> <br />{" "}
								{"Telefono: " + obj.telefono} <br />{" "}
								{"Direccion: " + obj.direccion} <br />{" "}
								{"Correo electronico: " + obj.correo}
							</Popup>
						</Marker>
					);
				})}
				<MyComponent />
				{latitud && longitud ? (
					<LocationMarker latlng={[latitud ?? 0, longitud ?? 0]} />
				) : (
					<></>
				)}
			</MapContainer>
		</div>
	);
};
function MyComponent() {
	const map = useMapEvents({
		click: () => {
			map.locate();
		},
		locationfound: (location) => {
			console.log("location found:", location);
		},
	});
	return null;
}

function LocationMarker(props) {
	const map = useMapEvents({
		click() {
			/* map.locate(); */
		},
		locationfound(e) {
			/* map.flyTo(e.latlng, map.getZoom()); */
		},
	});
	map.flyTo(props.latlng, map.getZoom());
	/* map.flyTo([0, 0], map.getZoom());
	setPosition([0, 0]); */

	return (
		<Marker position={props.latlng}>
			<Popup>You are here</Popup>
		</Marker>
	);
}

export default Mapa;
