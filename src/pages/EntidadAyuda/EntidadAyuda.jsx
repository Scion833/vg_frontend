import { useCallback, useMemo, useRef, useState } from "react";

import {
	BsFillPencilFill,
	BsFillTelephoneXFill,
	BsFillHouseCheckFill,
	BsEnvelopeAt,
	BsPinMapFill,
	BsPinMap,
} from "react-icons/bs";
import { DIRURL, urls_apis } from "../../models/coneccion/comunicacion";
import {
	MapContainer,
	Marker,
	Popup,
	TileLayer,
	useMapEvent,
} from "react-leaflet";
import { Form } from "react-bootstrap";

function DraggableMarker(props) {
	const [draggable, setDraggable] = useState(false);
	const [position, setPosition] = useState(props.position);
	const markerRef = useRef(null);
	const eventHandlers = useMemo(
		() => ({
			dragend() {
				const marker = markerRef.current;
				if (marker != null) {
					setPosition(marker.getLatLng());
					props.sendCoordinades(marker.getLatLng());
				}
			},
		}),
		// eslint-disable-next-line
		[]
	);
	const toggleDraggable = useCallback(() => {
		setDraggable((d) => !d);
		// eslint-disable-next-line
	}, []);

	return (
		<Marker
			draggable={draggable}
			eventHandlers={eventHandlers}
			position={position}
			ref={markerRef}
		>
			<Popup minWidth={90}>
				<span onClick={toggleDraggable}>
					{draggable
						? "La ubicacion ahora se puede arrastrar"
						: "Clic aqui para poder arrastrar la ubicacion"}
				</span>
			</Popup>
		</Marker>
	);
}

const EntidadAyuda = ({ ...props }) => {
	const center = {
		lat: -16.498870325232787,
		lng: -68.17432431951214,
	};
	const [position, setPosition] = useState(center);

	const refNombre = useRef(null);
	const refTelefono = useRef(null);
	const refDireccion = useRef(null);
	const refCorreo = useRef(null);
	const refTipo = useRef(null);
	const refLatitud = useRef(null);
	const refLongitud = useRef(null);

	const getEntidad = async () => {
		let bodyContent = new FormData();

		bodyContent.append("nombre", refNombre.current.value);
		bodyContent.append("telefono", refTelefono.current.value);
		bodyContent.append("direccion", refDireccion.current.value);
		bodyContent.append("correo", refCorreo.current.value);
		bodyContent.append("tipo", refTipo.current.value);
		bodyContent.append("latitud", refLatitud.current.value);
		bodyContent.append("longitud", refLongitud.current.value);

		await fetch(DIRURL.concat(urls_apis.CreateEntidad_Ayuda), {
			method: "POST",
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
			},
			body: bodyContent,
		})
			.then((res) => res.json())
			.then((data) => console.log(data))
			.catch((err) => console.log(err));

		return bodyContent;
	};

	const sendCoordinades = (latLng) => {
		refLatitud.current.value = latLng.lat;
		refLongitud.current.value = latLng.lng;
		setPosition(latLng);
	};

	const sendCoordinadesMark = (latLng) => {
		refLatitud.current.value = latLng.lat;
		refLongitud.current.value = latLng.lng;
	};

	return (
		<div className="container">
			<div className="row my-5 justify-content-center">
				<div className="col-12 col-md-9">
					<h1 className="display-2 fw-bold text-center text-info">
						Entidad Ayuda
					</h1>
				</div>
			</div>

			<div className="row justify-content-center">
				<div className="col-10 col-md-9 col-lg-11 col-xl-7">
					<div className="shadow-lg border border-secondary-subtle mb-5">
						<form
							onSubmit={(e) => e.preventDefault()}
							action=""
							className="was-validated my-4 mx-4"
						>
							<div className="row mb-4">
								<div className="col-7 col-lg-3 col-xl-3 col-xxl-3">
									<label
										htmlFor="inputNombre"
										className="col-form-label col-form-label-lg"
									>
										Nombre
										<BsFillPencilFill
											className="ms-2 mb-2"
											size={23}
										/>
									</label>
								</div>

								<div className="col-10 col-sm-12 col-lg-9 col-xl-9 col-xxl-9">
									<input
										ref={refNombre}
										type="text"
										className="form-control form-control-lg"
										id="inputNombre"
										placeholder="Juanito Perez"
										required
									/>
								</div>
							</div>

							<div className="row mb-4">
								<div className="col-8 col-lg-3 col-xl-3 col-xxl-3">
									<label
										htmlFor="inputTel"
										className="col-form-label col-form-label-lg"
									>
										Telefono
										<BsFillTelephoneXFill
											className="ms-2 mb-2"
											size={23}
										/>
									</label>
								</div>
								<div className="col-10 col-sm-12 col-lg-9 col-xl-9 col-xxl-9">
									<input
										ref={refTelefono}
										type="text"
										className="form-control form-control-lg"
										id="inputTel"
										placeholder="77774444"
										required
									/>
								</div>
							</div>

							<div className="row mb-4">
								<div className="col-8 col-lg-3 col-xl-4 col-xxl-3">
									<label
										htmlFor="inputDir"
										className="col-form-label col-form-label-lg"
									>
										Direccion
										<BsFillHouseCheckFill
											className="ms-2 mb-2"
											size={23}
										/>
									</label>
								</div>
								<div className="col-10 col-sm-12 col-lg-9 col-xl-8 col-xxl-9">
									<input
										ref={refDireccion}
										type="text"
										className="form-control form-control-lg"
										id="inputDir"
										placeholder="Zona Vino Tinto, calle Baltazar de Salas"
										required
									/>
								</div>
							</div>

							<div className="row mb-4">
								<div className="col-8 col-lg-3 col-xl-3 col-xxl-3">
									<label
										htmlFor="inputEmail"
										className="col-form-label col-form-label-lg"
									>
										Correo
										<BsEnvelopeAt
											className="ms-2 mb-2"
											size={23}
										/>
									</label>
								</div>
								<div className="col-10 col-sm-12 col-lg-9 col-xl-9 col-xxl-9">
									<input
										ref={refCorreo}
										type="email"
										className="form-control form-control-lg"
										id="inputEmail"
										placeholder="juanitoPerez@gmail.com"
										required
									/>
								</div>
							</div>

							<div className="row mb-4">
								<div className="col-8 col-lg-3 col-xl-3 col-xxl-3">
									<label
										htmlFor="inputCoord"
										className="col-form-label col-form-label-lg"
									>
										Tipo
									</label>
								</div>
								<div className="col-10 col-sm-12 col-lg-9 col-xl-9 col-xxl-9">
									<Form.Select
										aria-label="Default select"
										ref={refTipo}
									>
										<option>
											Seleccione el tipo de la Entidad
										</option>
										<option value={"Modulo Policial"}>
											Modulo Policial
										</option>
										<option value={"Centro de Ayuda"}>
											Centro de Ayuda
										</option>
										<option value={"Centro de Ayuda"}>
											Centro Medico
										</option>
									</Form.Select>
									{/* <input
										ref={refTipo}
										type="text"
										className="form-control form-control-lg"
										id="inputCoord"
										placeholder="escriba el tipo de Entidad"
										required
									/> */}
								</div>
							</div>

							<div className="row mb-4">
								<div className="col-8 col-lg-3 col-xl-4 col-xxl-3">
									<label
										htmlFor="inputLat"
										className="col-form-label col-form-label-lg"
									>
										Latitud
										<BsPinMapFill
											className="ms-2 mb-2"
											size={23}
										/>
									</label>
								</div>
								<div className="col-10 col-sm-12 col-lg-9 col-xl-8 col-xxl-5">
									<input
										ref={refLatitud}
										type="text"
										className="form-control form-control-lg"
										id="inputLat"
										placeholder="34° 36′ 30″ N"
										required
									/>
								</div>
							</div>

							<div className="row mb-4">
								<div className="col-8 col-lg-3 col-xl-4 col-xxl-3">
									<label
										htmlFor="inputLon"
										className="col-form-label col-form-label-lg"
									>
										Longitud
										<BsPinMap
											className="ms-2 mb-2"
											size={23}
										/>
									</label>
								</div>
								<div className="col-10 col-sm-12 col-lg-9 col-xl-8 col-xxl-5">
									<input
										ref={refLongitud}
										type="text"
										className="form-control form-control-lg"
										id="inputLon"
										placeholder="99° 7′ 39″ O"
										required
									/>
								</div>
							</div>
							<div
								style={{
									display: "grid",
									height: "400px",
									width: "100%",
								}}
							>
								<MapContainer
									center={[
										-16.498961301573583, -68.17430445633032,
									]}
									zoom={13}
								>
									<TileLayer
										attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
										url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
									/>
									<DraggableMarker
										key={position}
										position={position}
										sendCoordinades={sendCoordinadesMark}
									/>
									<MyComponent
										sendCoordinades={sendCoordinades}
									/>
								</MapContainer>
							</div>

							<div className="row my-4">
								<div className="col-12 d-flex justify-content-center">
									<button
										onClick={() => getEntidad()}
										className="btn btn-lg fs-2 text-white rounded"
										style={{
											backgroundColor: "steelblue",
										}}
									>
										Registrar
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
};

function MyComponent(props) {
	useMapEvent("click", (ev) => {
		props.sendCoordinades(ev.latlng);
	});
	return null;
}

export default EntidadAyuda;
