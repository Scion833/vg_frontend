
import { Alert, Button } from "react-bootstrap";

const MensajeAlerta = (props) => {
	return (
		<>
			<Alert show={props.show} variant="danger">
				<Alert.Heading>{props.titulo}</Alert.Heading>
				<p>{props.mensaje}</p>
				<hr />
				<div className="d-flex justify-content-end">
					<Button
						onClick={() => props.onClick()}
						variant="outline-danger"
					>
						Actualizar
					</Button>
				</div>
			</Alert>
		</>
	);
};

export default MensajeAlerta;
