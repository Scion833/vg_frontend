import { BsLink45Deg } from "react-icons/bs";

import "./estiloCard.css";

const ModeloArticulo = (props) => {
  return (
    <div className="" style={{ fontFamily: "Arial Narrow" }}>
      <div className="col">
        <div className="shadow rounded-5">
          <div
            className="card rounded-5 border-2 h-100"
          >
            <div className="card-img-top ratio ratio-4x3">
              <img alt="imagenArticulo" className="rounded-5" src={props.imagen}></img>
            </div>

            <div className="card-body">
              <div
                className="card-title fw-semibold fs-4"
                style={{ color: "#EC715E", lineHeight: "1.1em" }}
              >
                {props.titulo}
              </div>

              <div className="card-subtitle mt-2 mb-2 fs-6">
                <b className="" style={{ color: "#DCA7AE" }}>
                  Autor:{" "}
                </b>{" "}
                {props.autor}
              </div>

              <div className="card-subtitle mb-2 fs-6">
                <b className="" style={{ color: "#DCA7AE" }}>
                  Publicado:{" "}
                </b>
                {props.fechapub}
              </div>

              <span className="card-text text-center">
                <div
                  className="overflow-auto mb-1"
                  style={{ maxHeight: "70px" }}
                >
                  <p className="me-3 mb-0">{props.descripcion}</p>
                </div>
              </span>

              <div className="text-center">
                <a
                  className="card-link link-offset-2 link-offset-2-hover link-underline link-underline-opacity-0 link-underline-opacity-25-hover"
                  href={props.link}
                  style={{ color: "#B8A6BF" }}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Leer articulo completo.
                  <BsLink45Deg class="ms-1" size={16} />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModeloArticulo;
