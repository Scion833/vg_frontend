import ModeloArticulo from "./ModeloArticulo";
import ModeloTitulo from "../TituloPagina/ModeloTitulo";
import { useEffect, useState } from "react";
import {
	DIRURL,
	static_files,
	urls_apis,
} from "../../models/coneccion/comunicacion";
import MensajeAlerta from "./MensajeAlerta";
import { Spinner } from "react-bootstrap";

const Articulo = (props) => {
  const [arrayArticulos, setArrayArticulos] = useState([]);
  const [cargando, setCargando] = useState(false);
  let objMensaje = {
    show: false,
    titulo: "",
    mensaje: "",
  };
  const [mensaje, setMensaje] = useState(objMensaje);

  const getArticulos = async () => {
    setCargando(true);
		setMensaje({
			show: false,
			titulo: "Error al cargar",
			mensaje: "No se pudo obtener los datos",
		});
		await fetch(DIRURL.concat(urls_apis.getAllArticulo), {
			method: "GET",
			headers: {
				Accept: "*/*",
			},
		})
			.then((res) => {
				if (res.status === 200) {
					return res.json();
				} else {
					setCargando(false);
				}
			})
			.then((resultado) => {
				setCargando(false);
				setArrayArticulos(resultado?.data ?? []);
			})
			.catch((err) => {
				setCargando(false);
				let objMensaje = {
					show: true,
					titulo: "Error al cargar",
					mensaje: "No se pudo obtener los datos",
				};
				setMensaje(objMensaje);
			});
	};

  useEffect(() => {
    getArticulos();
  }, []);

  return (
    <div className="container">
      <ModeloTitulo titulo={"Artículos informativos"} />

      <div className="row row-cols-1 row-cols-md-3 g-5 mb-5">
        {cargando ? <Spinner animation="grow" variant="info" /> : <></>}
        <MensajeAlerta
          show={mensaje.show}
          titulo={mensaje.titulo}
          mensaje={mensaje.mensaje}
          onClick={getArticulos}
        />

        {arrayArticulos.map((obj, index) => (
          <ModeloArticulo
            key={index}
            titulo={obj.titulo}
            descripcion={obj.descripcion}
            link={obj.url_articulo}
            fechapub={obj.fecha}
            autor={obj.autor}
            imagen={
              DIRURL +
              static_files.path_articulo +
              "?nameImage=" +
              obj.url_imagen
            }
          />
        ))}
      </div>
    </div>
  );
};

export default Articulo;
