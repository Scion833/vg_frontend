import { useRef, useState } from "react";
import { Alert, Button, Container, Spinner, Stack } from "react-bootstrap";
import InputRowComponent from "../../../components/InputRowComponent/InputRowComponent";
import { DIRURL, urls_apis } from "../../../models/coneccion/comunicacion";

const InputDataArticulos = (props) => {
	const [isLoading, setIsLoading] = useState(false);
	const [messageStatus, setMessageStatus] = useState({
		title: "",
		message: "",
		isShow: false,
		variant: "",
	});

	const refTitulo = useRef(null);
	const refAutor = useRef(null);
	const refFecha = useRef(null);
	const refUrlArticulo = useRef(null);
	const refDescripcion = useRef(null);

	const objectArticulo = {
		titulo: "",
		autor: "",
		fecha: "",
		url_articulo: "",
		descripcion: "",
	};

	const saveArticulo = async () => {
		if (
			refTitulo.current.isValid() &&
			refAutor.current.isValid() &&
			refFecha.current.isValid() &&
			refUrlArticulo.current.isValid() &&
			refDescripcion.current.isValid()
		) {
			setIsLoading(true);

			objectArticulo.titulo = refTitulo.current.value();
			objectArticulo.autor = refAutor.current.value();
			objectArticulo.fecha = refFecha.current.value();
			objectArticulo.url_articulo = refUrlArticulo.current.value();
			objectArticulo.descripcion = refDescripcion.current.value();
			objectArticulo.id_articulo = props.id_articulo;
			
			await fetch(DIRURL.concat(urls_apis.updateArticulo), {
				method: "PUT",
				headers: {
					"x-token": localStorage.getItem("x-token"),
					Accept: "*/*",
					"Content-Type": "application/json",
				},
				body: JSON.stringify(objectArticulo),
			})
				.then((res) => {
					switch (res.status) {
						case 200:
							setMessageStatus({
								title: "Guardado!!",
								message: "Se guardo correctamente",
								isShow: true,
								variant: "success",
							});
							props.refreshDate(objectArticulo);
							return res.json();
						case 409:
							setMessageStatus({
								title: "Error 409",
								message: "Error al guardar los datos",
								isShow: true,
								variant: "danger",
							});
							break;
						case 401:
							setMessageStatus({
								title: "Error 401",
								message: "Usuario no autorizado",
								isShow: true,
								variant: "danger",
							});
							break;
						default:
							break;
					}
				})
				.then((data) => {
					console.log(data);
				})
				.catch((err) => {
					setMessageStatus({
						title: "Error",
						message: "Se produjo un error al enviar los datos",
						isShow: true,
						variant: "danger",
					});
					console.log(err);
				})
				.finally(() => {
					setIsLoading(false);
				});
		} else {
			console.log("No soy valido");
		}
	};

	return (
		<Container>
			<InputRowComponent
				value={props.titulo}
				ref={refTitulo}
				label={"Titulo:"}
			/>
			<InputRowComponent
				value={props.autor}
				ref={refAutor}
				label={"Autor:"}
			/>
			<InputRowComponent
				value={props.fecha}
				type="date"
				ref={refFecha}
				label={"Fecha:"}
			/>
			<InputRowComponent
				value={props.descripcion}
				ref={refDescripcion}
				label={"Descripcion:"}
			/>
			<InputRowComponent
				value={props.url_articulo}
				ref={refUrlArticulo}
				label={"Url Articulo:"}
			/>
			{messageStatus.isShow ? (
				<Alert
					className="mt-4"
					variant={messageStatus.variant}
					onClose={() => {
						setMessageStatus({
							title: "",
							message: "",
							isShow: false,
							variant: "",
						});
					}}
					dismissible
				>
					<Alert.Heading>{messageStatus.title}</Alert.Heading>
					<p>{messageStatus.message}</p>
				</Alert>
			) : (
				<></>
			)}
			<Stack gap={2} className="col-md-5 mx-auto mt-4">
				<Button
					variant="primary"
					onClick={saveArticulo}
					disabled={isLoading}
				>
					{isLoading ? <Spinner animation="grow" /> : <></>}
					Guardar
				</Button>
			</Stack>
		</Container>
	);
};

export default InputDataArticulos;
