//import {Card } from "react-bootstrap";
import { Alert, Button, Spinner, Stack } from "react-bootstrap";
import CardArticulo from "./CardArticulo";
import { useEffect, useState } from "react";
import { DIRURL, urls_apis } from "../../../models/coneccion/comunicacion";

const EditarArticulo = (props) => {
	const [arrayArticulo, setArrayArticulo] = useState([]);
	const [isLoading, setIsLoading] = useState(false);
	const [messageStatus, setMessageStatus] = useState({
		title: "",
		message: "",
		isShow: false,
		variant: "",
	});

	const getAllArticulo = async () => {
		setIsLoading(true);

		setMessageStatus({
			title: "",
			message: "",
			isShow: false,
			variant: "",
		});

		await fetch(DIRURL.concat(urls_apis.getAllArticulo), {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				Accept: "*/*",
			},
		})
			.then((res) => {
				switch (res.status) {
					case 200:
						return res.json();
					case 409:
						setMessageStatus({
							title: "Error 409",
							message: "Error al obtener datos",
							isShow: true,
							variant: "danger",
						});
						break;

					default:
						break;
				}
			})
			.then((data) => {
				setArrayArticulo(data.data);
			})
			.catch((err) => {
				console.log(err);
				setMessageStatus({
					title: "Error",
					message: "Error al intentar obtener los datos",
					isShow: true,
					variant: "danger",
				});
			})
			.finally(() => {
				setIsLoading(false);
				console.log("finalizo");
			});
	};

	useEffect(() => {
		getAllArticulo();
	}, []);

	return (
		<>
			<h1 style={{ textAlign: "center" }}>LISTADO DE ARTICULOS</h1>
			<div className="text-center">
				{isLoading ? (
					<Spinner animation="grow" variant="warning" />
				) : (
					<></>
				)}
				{messageStatus.isShow ? (
					<Alert variant="danger">
						<Alert.Heading>{messageStatus.title}</Alert.Heading>
						<p>{messageStatus.message}</p>
						<hr />
						<p className="mb-0">
							<Button
								variant="outline-danger"
								onClick={getAllArticulo}
							>
								Actualizar
							</Button>
						</p>
					</Alert>
				) : (
					<></>
				)}
			</div>

			{arrayArticulo.map((obj, index) => {
				return (
					<Stack className="col-md-9 mx-auto">
						<CardArticulo
							titulo={obj.titulo}
							fecha={obj.fecha}
							descripcion={obj.descripcion}
							url_articulo={obj.url_articulo}
							autor={obj.autor}
							id_articulo={obj.id_articulo}
							key={obj.id_articulo}
						/>
					</Stack>
				);
			})}
		</>
	);
};
export default EditarArticulo;
