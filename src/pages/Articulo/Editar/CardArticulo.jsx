import { Button, Card, Col, Row } from "react-bootstrap";
import ModalInput from "../../../components/ModalInput/ModalInput";
import InputDataArticulos from "./InputDataArticulos";
import { useState } from "react";

const CardArticulo = (props) => {
	const [fecha, setFecha] = useState(props.fecha);
	const [titulo, setTitulo] = useState(props.titulo);
	const [url_articulo, setUrl_articulo] = useState(props.url_articulo);
	const [descripcion, setDescripcion] = useState(props.descripcion);
	const [autor, setAutor] = useState(props.autor);

	const refreshDate = (articulo) => {
		setFecha(articulo.fecha);
		setTitulo(articulo.titulo);
		setAutor(articulo.autor);
		setUrl_articulo(articulo.url_articulo);
		setDescripcion(articulo.descripcion);
	};

	return (
		<>
			<Card className="mb-3">
				<Card.Body>
					<Row>
						<Col>
							<Card.Title>{titulo}</Card.Title>
							<Card.Subtitle className="mb-2 text-muted">
								Fecha de Publicacion :{fecha}
							</Card.Subtitle>
						</Col>

						<Col sm={2}>
							<ModalInput
								title={"Editar Articulo"}
								textcancel="Cerrar"
								isactiveacceptbtn={false}
								icononclick={(f) => (
									<Button variant="outline-info" onClick={f}>
										EDITAR
									</Button>
								)}
							>
								<InputDataArticulos
									titulo={titulo}
									fecha={fecha}
									url_articulo={url_articulo}
									descripcion={descripcion}
									autor={autor}
									id_articulo={props.id_articulo}
									refreshDate={refreshDate}
								/>
							</ModalInput>
						</Col>
						<Col sm={2}>
							<Button variant="outline-danger">ELIMINAR</Button>
						</Col>
					</Row>
				</Card.Body>
			</Card>
		</>
	);
};
export default CardArticulo;
