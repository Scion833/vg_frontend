import { useRef, useState } from "react";
import img6 from "../../../assets/images/img6.png";

import {
	BsFillPencilFill,
	BsFillFilePersonFill,
	BsLink45Deg,
	BsFillCalendarPlusFill,
	BsPencilSquare,
} from "react-icons/bs";
import { DIRURL, urls_apis } from "../../../models/coneccion/comunicacion";
import { Alert, Button, Spinner } from "react-bootstrap";

const RegistroArticulo = ({ ...props }) => {
	const [isLoading, setIsLoading] = useState(false);
	const [messageStatus, setMessageStatus] = useState({
		title: "",
		message: "",
		isShow: false,
		variant: "",
	});

	const refTitulo = useRef(null);
	const refAutor = useRef(null);
	const refFecha = useRef(null);
	const refUrlArticulo = useRef(null);
	const refDataFile = useRef(null);
	const refImgPreview = useRef(null);
	const refDescripcion = useRef(null);

	const getArticulo = async () => {
		let bodyContent = new FormData();

		bodyContent.append("titulo", refTitulo.current.value);
		bodyContent.append("autor", refAutor.current.value);
		bodyContent.append("fecha", refFecha.current.value);
		bodyContent.append("url_articulo", refUrlArticulo.current.value);
		bodyContent.append("archivo", refDataFile.current?.files[0] ?? null);
		bodyContent.append("descripcion", refDescripcion.current.value);

		setIsLoading(true);

		await fetch(DIRURL.concat(urls_apis.createArticulo), {
			method: "POST",
			headers: {
				"x-token": localStorage.getItem("x-token"),
				Accept: "*/*",
			},
			body: bodyContent,
		})
			.then((res) => {
				switch (res.status) {
					case 200:
						cleanInputs();
						setMessageStatus({
							title: "Se registro correctamente",
							message:
								'Ya es visible en Articulo, revise en "Ver Articulos".',
							isShow: true,
							variant: "success",
						});
						return res.json();
					case 409:
						setMessageStatus({
							title: "No se pudo completar el registro",
							message:
								"Revise su conexión o contactarse con soporte técnico",
							isShow: true,
							variant: "danger",
						});
						break;
					case 412:
						setMessageStatus({
							title: "No se pudo completar el registro",
							message: "Inserte una Imagen",
							isShow: true,
							variant: "danger",
						});

						break;
					default:
						break;
				}
			})
			.then((data) => console.log(data))
			.catch((err) => {
				console.log(err);
				setMessageStatus({
					title: "No se pudo completar el registro",
					message:
						"Revise su conexión o contactarse con soporte técnico",
					isShow: true,
					variant: "danger",
				});
			})
			.finally(() => {
				setIsLoading(false);
			});

		return bodyContent;
	};

	const onSelectImg = (e) => {
		let file = e.target.files[0];
		if (file) {
			const objectUrl = URL.createObjectURL(file);
			refImgPreview.current.src = objectUrl;
			//setSelectedFile(file);
			//reader.readAsDataURL(file);
		}
		//e.target.value = '';
	};

	const cleanInputs = () => {
		refTitulo.current.value = "";
		refAutor.current.value = "";
		refFecha.current.value = "";
		refUrlArticulo.current.value = "";
		refDataFile.current.value = "";
		refImgPreview.current.src = img6;
		refDescripcion.current.value = "";
	};

	return (
		<div className="container">
			<div className="row my-5 justify-content-center">
				<div className="col-12 col-md-9">
					<h1 className="display-2 fw-bold text-center text-info">
						Registro de Articulos
					</h1>
				</div>
			</div>

			<div className="row justify-content-center">
				<div className="col-12 col-md-8 col-lg-8 col-xl-7 col-xxl-6">
					<div className="shadow-lg border border-secondary-subtle mb-5">
						<form
							onSubmit={(e) => e.preventDefault()}
							className="was-validated my-4 mx-4"
						>
							<div className="row mb-4">
								<div className="col-12 col-lg-3 col-xl-3 col-xxl-3">
									<label
										htmlFor="inputTitulo"
										className="col-form-label col-form-label-lg"
									>
										Titulo
										<BsFillPencilFill
											className="ms-2 mb-2"
											size={23}
										/>
									</label>
								</div>
								<div className="col-12 col-sm-12 col-lg-9 col-xl-9 col-xxl-9">
									<input
										type="text"
										className="form-control form-control-lg"
										ref={refTitulo}
										placeholder="Articulo de la violencia"
										required
									/>
								</div>
							</div>

							<div className="row mb-4">
								<div className="col-12 col-lg-3 col-xl-3 col-xxl-3">
									<label
										htmlFor="inputAutor"
										className="col-form-label col-form-label-lg"
									>
										Autor
										<BsFillFilePersonFill
											className="ms-2 mb-2"
											size={23}
										/>
									</label>
								</div>
								<div className="col-12 col-sm-12 col-lg-9 col-xl-9 col-xxl-9">
									<input
										type="text"
										className="form-control form-control-lg"
										ref={refAutor}
										placeholder="Juanito Perez"
										required
									/>
								</div>
							</div>

							<div className="row mb-4 g-0">
								<div className="col-12 col-lg-6 col-xl-5 col-xxl-5">
									<label
										htmlFor="inputFecha"
										className="col-form-label col-form-label-lg"
									>
										Fecha del articulo
										<BsFillCalendarPlusFill
											className="ms-2 mb-2"
											size={23}
										/>
									</label>
								</div>
								<div className="col-10 col-sm-9 col-md-8 col-lg-6 col-xl-7 col-xxl-7">
									<input
										type="date"
										className="form-control form-control-lg"
										ref={refFecha}
										aria-describedby="ayuda-fecha"
										required
									/>
									<div id="ayuda-fecha" className="form-text">
										Indique la fecha por favor.
									</div>
								</div>
							</div>

							<div className="row mb-4">
								<div className="col-12 col-lg-4 col-xl-4 col-xxl-4">
									<label
										htmlFor="inputUrl"
										className="col-form-label col-form-label-lg"
									>
										Url articulo
										<BsLink45Deg
											className="ms-2 mb-2"
											size={23}
										/>
									</label>
								</div>
								<div className="col-12 col-sm-12 col-lg-8 col-xl-8 col-xxl-8">
									<input
										type="text"
										className="form-control form-control-lg"
										ref={refUrlArticulo}
										placeholder="htttps://www.example.com"
										required
									/>
								</div>
							</div>

							<div className="row mb-4">
								<div className="col-12 col-sm-12 text-end">
									<img
										src={img6}
										alt="imagen"
										ref={refImgPreview}
										className="img-thumbnail"
										style={{
											height: "350px",
											width: "600px",
										}}
									/>

									<div className="mt-4">
										<input
											type="file"
											className="form-control form-control-lg"
											ref={refDataFile}
											onChange={onSelectImg}
											aria-describedby="inputGroupFileAddon04"
											style={{ color: "steelblue" }}
											required
										/>
									</div>

									<div className="form-text text-start">
										Ingresa la imagen del articulo por
										favor.
									</div>
								</div>
							</div>

							<div className="row mb-4">
								<div className="col-12 col-sm-12">
									<div className="input-group input-group-lg">
										<label
											className="input-group-text"
											htmlFor="inputDesc"
										>
											Descripcion
											<BsPencilSquare
												className="ms-2"
												size={23}
											/>
										</label>
										<textarea
											className="form-control"
											ref={refDescripcion}
											rows="4"
											aria-label="Notas"
											required
										></textarea>
									</div>
								</div>
							</div>
							{/* btn btn-lg fs-2 text-white border border-2 rounded-1 */}
							<div className="row">
								{messageStatus.isShow ? (
									<Alert
										variant={messageStatus.variant}
										onClose={() =>
											setMessageStatus({
												title: "",
												message: "",
												isShow: false,
												variant: "",
											})
										}
										dismissible
									>
										<Alert.Heading>
											{messageStatus.title}
										</Alert.Heading>
										<p>{messageStatus.message}</p>
									</Alert>
								) : (
									<></>
								)}
								<div className="col-12 d-flex justify-content-start">
									<Button
										className="btn btn-lg fs-2 text-white border border-2 rounded-1"
										style={{
											backgroundColor: "steelblue",
										}}
										size="lg"
										onClick={() => {
											console.log(getArticulo());
										}}
										disabled={isLoading}
									>
										{isLoading ? (
											<Spinner
												animation="grow"
												variant="light"
											/>
										) : (
											<></>
										)}
										Registrar
									</Button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
};

export default RegistroArticulo;
