import { Col, Container, Row, Stack } from "react-bootstrap";
import NavbarMain from "./NavbarMain";
import "bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-router-dom";
import jwtDecode from "jwt-decode";
import { useEffect, useState } from "react";
import { BiEdit, BiSolidMapPin } from "react-icons/bi";
import { MdContactEmergency, MdEventRepeat, MdOutlineEventNote } from "react-icons/md";
import { RiArticleLine } from "react-icons/ri";
import { PiArticleDuotone } from "react-icons/pi";
import AccordionLink from "../../components/AccordionLinks/AccordionLink";
import { DIRURL } from "../coneccion/comunicacion";
import io from "socket.io-client";
import ToastAlert from "../../components/ToastAlert/ToastAlert";

const Layout = ({ ...props }) => {
	const [pageList, setPageList] = useState([]);
	const [arrayAlerts, setArrayAlerts] = useState([]);
	const socket = io(DIRURL, {
		autoConnect: false,
	});

	socket.on("connect", () => {
		console.log("Conectado socket");
	});
	socket.on("disconnect", () => {
		console.log("cliente desconectado");
	});
	socket.on("emitAlert", async (data) => {
		//console.log("Nueva alerta: ", data);

		let alert = {
			title: "alerta",
			description: data.mensaje,
			latitud: data.latitud,
			longitud: data.longitud,
			id_usuario: data.id_usuario,
		};
		//const newArrayAlerts = await [...arrayAlerts, alert];

		setArrayAlerts((prevArray) => [...prevArray, alert]);
	});

	const addOption = () => {
		const token = localStorage.getItem("x-token");
		let pages = [
			{
				name: "Orientate",
				url: "/Orientate",
			},
			{
				name: "Ver Articulos",
				url: "/articulo",
				icon: <PiArticleDuotone size={"30px"} />,
			},
			{
				name: "Ver Mapa para Entidad Ayuda",
				url: "/EntidadAyuda/Mapa",
				icon: <BiSolidMapPin size={"30px"} />,
			},
			{
				name: "Preguntas Frecuentes",
				url: "/ChatBot",
			},
		];
		if (token) {
			let perfiles = jwtDecode(token).perfiles;

			const adminUrl = [
				{
					name: "Añadir Administrador",
					url: "/RegistrarAdmin",
				},
				{
					name: "Seccion Articulo",
					subList: [
						{
							name: "Registrar Articulo",
							url: "/articulo/registroArticulo",
							icon: <RiArticleLine size={"30px"} />,
						},
						{
							name: "Editar Articulo",
							url: "/Articulo/EditarArticulo",
							icon: <BiEdit size="30px" />,
						},
					],
				},
				{
					name: "Seccion Eventos",
					subList: [
						{
							name: "Registrar Evento",
							url: "/eventos/registroEvento",
							icon: <MdOutlineEventNote size={"30px"} />,
						},
						{
							name: "Administrar Evento",
							url: "/eventos/administrarEventos",
							icon: <MdEventRepeat size={"30px"} />,
						},
					],
				},
				{
					name: "Seccion Orientate",
					subList: [
						{
							name: "Editar Orientate",
							url: "/Orientate/EditarOrientate",
						},
						{
							name: "Registrar Orientate",
							url: "/orientate/registroOrientate",
						},
					],
				},
				{
					name: "Registrar Entidad Ayuda",
					url: "/entidad/EntidadAyuda",
				},

				{
					name: "Usuarios",
					url: "/Usuarios",
					icon: <MdEventRepeat size={"30px"} />,
				},
				{
					name: "Registrar Preguntas y Respuestas",
					url: "/ChatBot/RegistroChatBot",
				},
			];

			const userUrl = [
				{
					name: "Eventos",
					url: "/eventos",
				},
				{
					name: "Denuncia",
					url: "/perfil/usuario/denuncia",
				},
				{
					name: "Mis Contactos",
					url: "/MostrarContacto",
					icon: <MdContactEmergency size={"30px"} />,
				},
			];

			for (let i = 0; i < perfiles.length; i++) {
				if (perfiles[i].perfil === "administrador") {
					socket.connect();

					pages = pages.concat(adminUrl);
				}
				if (perfiles[i].perfil === "usuario") {
					pages = pages.concat(userUrl);
				}
			}
		}
		setPageList(pages);
	};

	useEffect(() => {
		addOption();

		return () => {
			socket.disconnect();
		};
		// eslint-disable-next-line
	}, []);
	return (
		<>
			<Stack gap={0}>
				<NavbarMain
					pageList={pageList}
					isLogin={props.isLogin}
					logout={props.logout}
				/>

				<Container fluid={true}>
					<Row>
						<Col
							sm={2}
							className="border-end bg-body-tertiary reactive-menu"
							style={{
								overflowY: "auto",
								height: "calc(100vh - 56px)",
							}}
						>
							{pageList.map((value, index) =>
								value?.subList ? (
									<AccordionLink
										key={"bar" + index}
										name={value.name}
										list={value.subList}
									/>
								) : (
									<Row
										className="btn-ayuda text-start text-decoration-none reactive-menu"
										as={Link}
										key={index}
										to={value.url}
									>
										<Col>
											<p className="text-ayuda text-dark-emphasis">
												{value.name}
											</p>
										</Col>
										<Col lg="2">{value.icon ?? <></>}</Col>
									</Row>
								)
							)}
							{/* <div className="border-top">
								<h3>Creditos:</h3>
								<ListGroup>
									<ListGroup.Item>
										Alex Wilmer Luna Choque
									</ListGroup.Item>
									<ListGroup.Item>
										Luis Fernando Ticona Huacani
									</ListGroup.Item>
									<ListGroup.Item>
										Ruth Abigail Carvajal Alcon
									</ListGroup.Item>
									<ListGroup.Item>
										Wilma Limachi Casi
									</ListGroup.Item>
									<ListGroup.Item>
										Marcelo Ovando Colque
									</ListGroup.Item>
									<ListGroup.Item>
										Christian Marban Callisaya
									</ListGroup.Item>
								</ListGroup>
							</div> */}
						</Col>
						<Col
							style={{
								overflowY: "auto",
								height: "calc(100vh - 56px)",
							}}
						>
							{props.children}
						</Col>
						<ToastAlert
							key={arrayAlerts.length}
							arrayalerts={arrayAlerts}
							onClose={(index) => {
								setArrayAlerts((prevArray) =>
									prevArray.filter((_, i) => i !== index)
								);
							}}
						/>
					</Row>
				</Container>
			</Stack>
		</>
	);
};

export default Layout;
