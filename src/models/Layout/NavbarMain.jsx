import { useEffect, useState } from "react";
import {
	Button,
	NavDropdown,
	Offcanvas,
} from "react-bootstrap";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { Link } from "react-router-dom";
import { MdDarkMode, MdLightMode } from "react-icons/md";
import jwtDecode from "jwt-decode";
import AccordionLinkMovil from "../../components/AccordionLinks/AccordionLinkMovil";

function NavbarMain(props) {
	const [themeMode, setThemeMode] = useState("light");
	const [themeIcon, setThemeIcon] = useState(<MdDarkMode />);
	const [offcanvasOpen, setOffcanvasOpen] = useState(false);

	const btnTheme = () => {
		setThemeIcon(themeMode !== "light" ? <MdLightMode /> : <MdDarkMode />);
		setThemeMode(themeMode !== "light" ? "light" : "dark");
		localStorage.setItem("theme", themeMode === "light" ? "light" : "dark");
		document.documentElement.setAttribute("data-bs-theme", themeMode);
	};

	const makePerfiles = () => {
		let perfiles = jwtDecode(localStorage.getItem("x-token"))?.perfiles;
		if (perfiles) {
			let arrObj = [];
			for (let i = 0; i < perfiles.length; i++) {
				arrObj.push(
					<NavDropdown.Item
						as={Link}
						onClick={() => {
							setOffcanvasOpen(false);
						}}
						key={"perfiles" + i}
						to={`/perfil/${encodeURIComponent(
							perfiles[i].perfil
						)}?id=${encodeURIComponent(perfiles[i].id)}`}
					>
						{perfiles[i].perfil}
					</NavDropdown.Item>
				);
			}
			return arrObj;
		} else {
			return <></>;
		}
	};

	useEffect(() => {
		const getStoredTheme = () => localStorage.getItem("theme");
		if (getStoredTheme()) {
			setThemeIcon(
				getStoredTheme() !== "light" ? <MdLightMode /> : <MdDarkMode />
			);
			setThemeMode(getStoredTheme() !== "light" ? "light" : "dark");
			document.documentElement.setAttribute(
				"data-bs-theme",
				getStoredTheme()
			);
		} else {
			localStorage.setItem("theme", "light");
		}
		//setThemeMode()
	}, []);
	return (
		<Navbar expand={"sm"} className="bg-body-tertiary">
			<Container fluid>
				<Navbar.Brand href="#">Violencia de Genero</Navbar.Brand>
				<Navbar.Toggle
					aria-controls={`offcanvasNavbar-expand-sm`}
					onClick={() => {
						setOffcanvasOpen(true);
					}}
				/>
				<Navbar.Offcanvas
					id={`offcanvasNavbar-expand-sm`}
					aria-labelledby={`offcanvasNavbarLabel-expand-sm`}
					placement="start"
					show={offcanvasOpen}
				>
					<Offcanvas.Header
						closeButton
						onHide={() => setOffcanvasOpen(false)}
					>
						<Offcanvas.Title id={`offcanvasNavbarLabel-expand-sm`}>
							Menu
						</Offcanvas.Title>
					</Offcanvas.Header>
					<Offcanvas.Body>
						<Nav className="justify-content-end flex-grow-1 pe-3">
							{props.pageList.map((obj, index) =>
								obj?.subList ? (
									<AccordionLinkMovil
										key={"accordionMovil"+index}
										clase={"reactive-navbar"}
										name={obj.name}
										list={obj.subList}
										offcanvas={()=>setOffcanvasOpen(false)}
									/>
								) : (
									<Nav.Link
										className="reactive-navbar"
										key={"nav"+index}
										as={Link}
										onClick={() => {
											setOffcanvasOpen(false);
										}}
										to={obj.url}
									>
										{obj.icon ?? <></>}
										{obj.name}
									</Nav.Link>
								)
							)}
							<Nav.Link
								as={Link}
								to={"/"}
								onClick={() => {
									setOffcanvasOpen(false);
								}}
							>
								Home
							</Nav.Link>
							{props.isLogin ? (
								<>
									<NavDropdown title="Perfiles">
										{makePerfiles()}
									</NavDropdown>
									<Nav.Link
										as={Link}
										className="nav-link text-danger"
										to={"/"}
										onClick={() => {
											setOffcanvasOpen(false);
											props.logout();
										}}
									>
										Cerrar Sesion
									</Nav.Link>
								</>
							) : (
								<Nav.Link
									as={Link}
									to={"/login"}
									onClick={() => {
										setOffcanvasOpen(false);
									}}
								>
									Iniciar Sesion
								</Nav.Link>
							)}
							<Button
								variant="outline-secondary"
								onClick={btnTheme}
							>
								{themeIcon}
							</Button>
						</Nav>
					</Offcanvas.Body>
				</Navbar.Offcanvas>
			</Container>
		</Navbar>
	);
}

export default NavbarMain;
