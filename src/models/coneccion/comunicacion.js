export const DIRURL = "";

export const urls_apis = {
	//usuario
	registrar: "/api/v1/registrar",
	loginUsuario: "/api/v1/loginUsuario",
	getAllUsuarios: "/api/v1/getAllUsuarios",
	getUsuarioByIDForAlert: "/api/v1/getUsuarioByIDForAlert",
	//identidad
	getAllIndentidad: "/api/v1/getAllIndentidad",
	//administrador
	createAdministrador: "/api/v1/createAdministrador",
	//denuncia
	addDenuncia: "/api/v1/addDenuncia",
	getDenunciaByIdUsuario: "/api/v1/getDenunciaByIdUsuario",
	//agresor
	addAgresor: "/api/v1/AddAgresor",
	getAgresorByIdDenuncia: "/api/vi/getAgresorByIdDenuncia",
	//victima
	addVictima: "/api/v1/addVictima",
	getVictimaByIDdenuncia: "/api/v1/getVictimaByIDdenuncia",
	//prueba
	addPrueba: "/api/v1/addPrueba",
	getPruebaByIdUsuarioAndDenuncia: "/api/v1/getPruebaByIdUsuarioAndDenuncia",
	addDenunciaHasPrueba: "/api/v1/addDenunciaHasPrueba",
	getPruebaAgregadaByIdUsuarioAndDenuncia:
		"/api/v1/getPruebaAgregadaByIdUsuarioAndDenuncia",
	deleteDenunciaHasPrueba: "/api/v1/deleteDenunciaHasPrueba",
	//articulo
	createArticulo: "/api/v1/createArticulo",
	getAllArticulo: "/api/v1/getAllArticulo",
	updateArticulo: "/api/v1/updateArticulo",
	//Entidad_Ayuda
	CreateEntidad_Ayuda: "/api/v1/createEntidad_Ayuda",
	GetAllEntidad_Ayuda: "/api/v1/getAllEntidad_Ayuda",
	AddEvento: "/api/v1/addEvento",
	//eventos
	GetAllEvento: "/api/v1/GetAllEvento",
	GetEventsToAttend: "/api/v1/getEventsToAttend",
	AddUsuarioHasEvento: "/api/v1/addUsuarioHasEvento",
	DeleteUsuarioHasEvento: "/api/v1/deleteUsuarioHasEvento",
	GetUsuarioByEvento: "/api/v1/getUsuarioByEvento",
	HabilitarEvento: "/api/v1/habilitarEvento",
	DeshabilitarEvento: "/api/v1/deshabilitarEvento",
	//contacto
	createContacto: "/api/v1/createContacto",
	getAllContacto: "/api/v1/getAllContacto",
	getAllContactoByUser: "/api/v1/getAllContactoByUser",
	updateContacto: "/api/v1/updateContacto",
	//orientate
	getAllOrientate: "/api/v1/getAllOrientate",
	addOrientate: "/api/v1/addOrientate",
	updateOrientate: "/api/v1/updateOrientate",
	//updateContacto: "/api/v1/updateContacto",

	//ChatBot
	AddChatBot: "/api/v1/addBancoPR",
	GetAllChatBot: "/api/v1/getAllBancoPR",
};

export const headers = {
	"x-token": localStorage.getItem("x-token"),
	Accept: "*/*",
	"Content-Type": "application/json",
};

export const static_files = {
	path_pruebas: "/uploads/pruebas",
	path_articulo: "/uploads/articulos",
};
