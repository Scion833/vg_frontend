import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Layout from "./models/Layout/Layout";
import Prevenir from "./pages/Prevenir/Prevenir";
import Login from "./pages/login/login";
import Usuarios from "./pages/usuarios/Usuarios";


import Registrar from "./pages/Registrar/Registrar";
import RegistrarAdmin from "./pages/RegistrarAdmin/RegistrarAdmin";
import { useEffect, useState } from "react";
import jwtDecode from "jwt-decode";
import Usuario from "./pages/perfil/Usuario/Usuario";
import Administrador from "./pages/perfil/Administrador/Administrador";
import Denuncia from "./pages/perfil/Usuario/Denuncia/Denuncia";

import Articulo from "./pages/Articulo/Articulo";
import EntidadAyuda from "./pages/EntidadAyuda/EntidadAyuda";
import RegistroOrientate from "./pages/Orientate/RegistroOrientate/RegistroOrientate";
import Eventos from "./pages/Eventos/Eventos";
import RegistroArticulo from "./pages/Articulo/RegistroArticulo/RegistroArticulo";
import RegistroEvento from "./pages/Eventos/RegistroEvento/RegistroEvento";

import EditarArticulo from "./pages/Articulo/Editar/EditarArticulo";
import EditarOrientate from "./pages/Orientate/EditarOrientate/EditarOrientate";
import Mapa from "./pages/EntidadAyuda/Mapa";
import Orientate from "./pages/Orientate/MostrarOrientate/Orientate";

import AdministrarEventos from "./pages/Eventos/AdministrarEventos/AdministrarEventos";

import Contacto from "./pages/Contacto/Contacto";
import TableContactos from "./pages/Contacto/MostrarContacto";
import NuevoContacto from "./pages/Contacto/NuevoContacto";

import ChatBot from "./pages/ChatBot/MostrarChatBot/ChatBot";
import RegistroChatBot from "./pages/ChatBot/RegistroChatBot/RegistroChatBot";
import Inicio from "./pages/Inicio/Inicio";







function App() {
	const [token, setToken] = useState(localStorage.getItem("x-token"));
	const [isLogin, setIsLogin] = useState(false);

	useEffect(() => {
		const handleStorageChange = () => {
			const newToken = localStorage.getItem("x-token");
			if (newToken && newToken!=='undefined') {
				let exp = jwtDecode(newToken)?.exp;
				if (Date.now() >= exp * 1000) {
					localStorage.removeItem("x-token");
					setIsLogin(false);
					setToken(null);
					return;
				}else{
					setIsLogin(true);
				}
			}
		};
		handleStorageChange();
		window.addEventListener("storage", handleStorageChange);

		return () => {
			window.removeEventListener("storage", handleStorageChange);
		};
	}, [token, isLogin]);

	return (
		<BrowserRouter>
			<Layout
				key={isLogin}
				isLogin={isLogin}
				logout={() => {
					localStorage.removeItem("x-token");
					setIsLogin(false);
					setToken(null);
				}}
			>
				<Routes>
					<Route exact path="/" element={<Inicio />} />
					<Route exact path="/Prevenir" element={<Prevenir />} />
					<Route exact path="/Login" element={<Login updateToken={(token) => {
															setIsLogin(token !== null);
															setToken(token);}} />} />
					<Route exact path="/Usuarios" element={<Usuarios />} />
					<Route exact path="/Registrar" element={<Registrar />} />
					<Route exact path="/RegistrarAdmin" element={<RegistrarAdmin />} />
					<Route path="/perfil/usuario" element={<Usuario/>}/>
					<Route path="/perfil/usuario/denuncia" element={<Denuncia />} />
					<Route path="/perfil/administrador" element={<Administrador />} />

					<Route path="/articulo" element={ <Articulo />} />
					<Route path="/articulo/registroArticulo" element= {<RegistroArticulo />}/>
					<Route path="/eventos" element={<Eventos/>} />
					<Route path="/eventos/registroEvento" element={ <RegistroEvento />} />
					<Route path="/eventos/administrarEventos" element={<AdministrarEventos/>} />
					<Route path="/entidad/EntidadAyuda" element={ <EntidadAyuda />} />
					<Route path="/orientate/registroOrientate" element={ <RegistroOrientate />} />

					<Route path="/Articulo/EditarArticulo" element={<EditarArticulo />} />
					<Route path="/Orientate/EditarOrientate" element ={<EditarOrientate/>} />
					<Route path="/EntidadAyuda/Mapa" element ={<Mapa/>}/>
					<Route path="/Orientate" element={ <Orientate />} />

					<Route path="/Contacto" element={<Contacto/>}/>
					<Route path="/MostrarContacto" element={<TableContactos/>}/>
					<Route path="/NuevoContacto" element={<NuevoContacto/>}/>
					<Route path="/ChatBot" element={<ChatBot/>}/>
					<Route path="/ChatBot/RegistroChatBot" element={<RegistroChatBot/>}/>
					
					

					<Route path="/Inicio" element={<Inicio/>}/>

				</Routes>
			</Layout>
		</BrowserRouter>
	);
}

export default App;


//estamos aqui